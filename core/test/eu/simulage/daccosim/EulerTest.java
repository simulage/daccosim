/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim;

import eu.simulage.daccosim.model.Graph;
import org.junit.Ignore;
import org.junit.Test;

import static eu.simulage.daccosim.HelperTest.checkHeatTransfer;
import static eu.simulage.daccosim.Stepper.Method.Euler;
import static eu.simulage.daccosim.view.Graphs.simpleHeatTransfer;

public class EulerTest {

    @Test
    public void cosimulation_speed_test() {
        Graph graph = simpleHeatTransfer()
                .settings().stopTime(86400).coInitialize()
                .stepper().method(Euler).minStep(1).stepSize(10).stepperVariables("sinus.Text", "wall.Q_b", "wall.Q_a", "zoneA.Tin", "zoneB.Tin")
                .commit();
        GraphExecutor executor = new GraphExecutor(graph);
        long init = System.nanoTime();
        executor.execute();
        executor.logFile().delete();
        long end = System.nanoTime();
        System.out.println(((end - init) / 1e9));
    }

    @Test
    @Ignore
    public void simple_heat_transfer() {
        Graph graph = simpleHeatTransfer()
                .settings().stopTime(86400).coInitialize()
                .stepper().method(Euler).minStep(1).stepSize(10).stepperVariables("sinus.Text", "wall.Q_b", "wall.Q_a", "zoneA.Tin", "zoneB.Tin")
                .commit();
        GraphExecutor executor = new GraphExecutor(graph);
        executor.load();
        executor.init();
        checkHeatTransfer(executor, 0.0,283.15, 293.15,293.15, 283.15,293.15, 283.15,293.15);
        executor.firstStep();
        checkHeatTransfer(executor, 10.0,283.15727220457563, 293.15,293.15, 283.15,293.1162095811426, 283.15,293.1162095811426);
        executor.step();
        checkHeatTransfer(executor, 20.0,283.1645444053054, 293.1162095811426,293.1162095811426, 283.15727220457563,293.0825355769544, 283.15727220457563,293.0825355769544);
        executor.step();
        checkHeatTransfer(executor, 62.70682342348835,283.19560153079317, 293.0825355769544,293.0825355769544, 283.1645444053054,292.94141736788146, 283.1645444053054,292.94141736788146);
        executor.step();
        checkHeatTransfer(executor, 105.4136468469767,283.22665821642966, 292.94141736788146,292.94141736788146, 283.19560153079317,292.80248740615275, 283.19560153079317,292.80248740615275);
        executor.step();
        checkHeatTransfer(executor, 156.85603845004317,283.26406645640213, 292.80248740615275,292.80248740615275, 283.22665821642966,292.6369086519978, 283.22665821642966,292.6369086519978);
        executor.step();
        checkHeatTransfer(executor, 183.09549653210476,283.2831468681228, 292.6369086519978,292.6369086519978, 283.26406645640213,292.55450416119146, 283.26406645640213,292.55450416119146);
        executor.step();
        checkHeatTransfer(executor, 216.7521052723788,283.30762005179605, 292.55450416119146,292.55450416119146, 283.2831468681228,292.4503617792489, 283.2831468681228,292.4503617792489);
        executor.step();
        checkHeatTransfer(executor, 273.71162794764314,283.3490355691426, 292.4503617792489,292.4503617792489, 283.30762005179605,292.275902374349, 283.30762005179605,292.275902374349);
        executor.step();
        checkHeatTransfer(executor, 301.8938188044438,283.36952574442927, 292.275902374349,292.275902374349, 283.3490355691426,292.1916902387985, 283.3490355691426,292.1916902387985);
        executor.step();
        checkHeatTransfer(executor, 337.0870186298765,283.3951120470444, 292.1916902387985,292.1916902387985, 283.36952574442927,292.0883866911403, 283.36952574442927,292.0883866911403);
        executor.step();
        checkHeatTransfer(executor, 386.8778695338896,283.43130841092017, 292.0883866911403,292.0883866911403, 283.3951120470444,291.9436205512411, 283.3951120470444,291.9436205512411);
    }

}
