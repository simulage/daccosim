/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim;

import eu.simulage.daccosim.model.Graph;
import org.junit.Ignore;
import org.junit.Test;

import static eu.simulage.daccosim.HelperTest.checkHeatTransfer;
import static eu.simulage.daccosim.Stepper.Method.AdamsBashforth;
import static eu.simulage.daccosim.view.Graphs.simpleHeatTransfer;


public class AdamsBashforthTest {

	@Test
	public void cosimulation_speed_test() {
		Graph graph = simpleHeatTransfer()
				.settings().stopTime(86400).coInitialize()
				.stepper().method(AdamsBashforth).order(3).minStep(1).stepSize(10).stepperVariables("sinus.Text", "wall.Q_b", "wall.Q_a", "zoneA.Tin", "zoneB.Tin")
				.commit();
		GraphExecutor executor = new GraphExecutor(graph);
		long init = System.nanoTime();
		executor.execute();
		long end = System.nanoTime();
		executor.logFile().delete();
		System.out.println(((end - init) / 1e9));
	}

	@Test
	@Ignore
	public void simple_heat_transfer() {
		Graph graph = simpleHeatTransfer()
				.settings().stopTime(86400).coInitialize()
				.stepper().method(AdamsBashforth).order(3).minStep(1).stepSize(10).stepperVariables("sinus.Text", "wall.Q_b", "wall.Q_a", "zoneA.Tin", "zoneB.Tin")
				.commit();
		GraphExecutor executor = new GraphExecutor(graph);
		executor.load();
		executor.init();
		checkHeatTransfer(executor, 0.0, 283.15, 293.15, 293.15, 283.15, 293.15, 283.15, 293.15);
		executor.firstStep();
		checkHeatTransfer(executor, 10.0, 283.15727220457563, 293.15, 293.15, 283.15, 293.1162095811426, 283.15, 293.1162095811426);
		executor.step();
		checkHeatTransfer(executor, 20.0, 283.1645444053054, 293.1162095811426, 293.1162095811426, 283.15727220457563, 293.0825355769544, 283.15727220457563, 293.0825355769544);
		executor.step();
		executor.step();
		executor.step();
		checkHeatTransfer(executor, 41.0, 283.179815997211, 293.0154476399768, 293.0154476399768, 283.1790887798436, 293.0121113819337, 283.1790887798436, 293.0121113819337);
		executor.step();
		checkHeatTransfer(executor, 42.0, 283.1805432144207, 293.0121113819337, 293.0121113819337, 283.179815997211, 293.0087784346269, 283.179815997211, 293.0087784346269);
		executor.step();
		executor.step();
		executor.step();
		checkHeatTransfer(executor, 45.0, 283.18272486506527, 293.00211479999024, 293.00211479999024, 283.1819976483517, 292.9987840137069, 283.1819976483517, 292.9987840137069);
		executor.step();
		checkHeatTransfer(executor, 46.0, 283.18345208160576, 292.9987840137069, 292.9987840137069, 283.18272486506527, 292.99545665415894, 283.18272486506527, 292.99545665415894);
		executor.step();
		executor.step();
		executor.step();
		checkHeatTransfer(executor, 49.0, 283.18563373015047, 292.9888041911886, 292.9888041911886, 283.1849065141522, 292.9854788690827, 283.1849065141522, 292.9854788690827);
		executor.step();
		checkHeatTransfer(executor, 50.0, 283.18636094596025, 292.9854788690827, 292.9854788690827, 283.18563373015047, 292.98215708402944, 283.18563373015047, 292.98215708402944);
		executor.step();
		executor.step();
		executor.step();
		checkHeatTransfer(executor, 53.0, 283.1885425922205, 292.97551576621026, 292.97551576621026, 283.18781537699914, 292.97219590083716, 283.18781537699914, 292.97219590083716);
		executor.step();
		checkHeatTransfer(executor, 54.0, 283.189269807238, 292.97219590083716, 292.97219590083716, 283.1885425922205, 292.968879677814, 283.1885425922205, 292.968879677814);
		executor.step();
		executor.step();
		executor.step();
		checkHeatTransfer(executor, 57.0, 283.1914514510292, 292.962249480229, 292.962249480229, 283.19072423664625, 292.958935064245, 283.19072423664625, 292.958935064245);
	}


}
