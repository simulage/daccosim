/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim;

import eu.simulage.daccosim.model.Graph;
import eu.simulage.daccosim.model.nodes.ExternalInput;
import eu.simulage.daccosim.model.nodes.operators.Gain;
import eu.simulage.daccosim.model.nodes.operators.Offset;
import eu.simulage.daccosim.view.Graphs;
import eu.simulage.daccosim.view.Helper;
import org.junit.Test;

import static eu.simulage.daccosim.DataType.Real;
import static eu.simulage.daccosim.Stepper.Method.ConstantStep;
import static eu.simulage.daccosim.view.Graphs.fmuPath;
import static eu.simulage.daccosim.view.Helper.*;
import static org.junit.Assert.*;

@SuppressWarnings("SpellCheckingInspection")
public class DngFileTest {

	@SuppressWarnings("ConstantConditions")
	@Test
	public void should_parse_text_correctly() {
		String text = "" +
				"FMU Tank " + fmuPath("TankGwin3264") + "\n" +
				"Output Tank outFlow Real\n" +
				"Input Tank Valve Boolean\n" +
				"FMU Barrel " + fmuPath("BarrelGwin3264") + "\n" +
				"Output Barrel Water Real\n" +
				"Input Barrel inFlow Real\n" +
				"Input Barrel Size Integer\n" +
				"Input Barrel Abort Boolean\n" +
				"FMU C1 " + fmuPath("C1win3264") + "\n" +
				"Output C1 Valve Boolean\n" +
				"FMU C2 " + fmuPath("C2win3264") + "\n" +
				"Output C2 Abort Boolean\n" +
				"Output C2 Size Integer\n" +
				"ExternalOutput water\n" +
				"Input water input Real\n" +
				"Connection C1.Valve Tank.Valve\n" +
				"Connection C2.Abort Barrel.Abort\n" +
				"Connection C2.Size Barrel.Size\n" +
				"Connection Tank.outFlow Barrel.inFlow\n" +
				"Connection Barrel.Water water.input\n" +
				"ConstantStepper 0.01\n" +
				"Simulation 0 18\n";
		Graph graph = DngFile.importDng(text).commit();
		assertEquals(5, graph.nodes().size());
		assertEquals(1, graph.node("Tank").inputs().size());
		assertEquals(1, graph.node("Tank").outputs().size());
		assertEquals(Real, graph.variable("Tank.outFlow").type());
		assertEquals(DataType.Boolean, graph.variable("Tank.Valve").type());
		assertEquals(3, graph.node("Barrel").inputs().size());
		assertEquals(1, graph.node("Barrel").outputs().size());
		assertEquals(Real, graph.variable("Barrel.Water").type());
		assertEquals(Real, graph.variable("Barrel.inFlow").type());
		assertEquals(DataType.Integer, graph.variable("Barrel.Size").type());
		assertEquals(DataType.Boolean, graph.variable("Barrel.Abort").type());
		assertEquals(0, graph.node("C1").inputs().size());
		assertEquals(1, graph.node("C1").outputs().size());
		assertEquals(DataType.Boolean, graph.variable("C1.Valve").type());
		assertEquals(0, graph.node("C2").inputs().size());
		assertEquals(2, graph.node("C2").outputs().size());
		assertEquals(DataType.Boolean, graph.variable("C2.Abort").type());
		assertEquals(DataType.Integer, graph.variable("C2.Size").type());
		assertEquals(5, graph.arrows().size());

		GraphExecutor graphExecutor = new GraphExecutor(graph);
		graphExecutor.load();
		graphExecutor.init();
		graphExecutor.firstStep();
		for (int i = 0; i < 50; i++) graphExecutor.step();
		assertEquals(0.0, (double) graphExecutor.graph().node("water").input("input").value(), 1e-4);
		graphExecutor.step();
		assertEquals(0.021186451, (double) graphExecutor.graph().node("water").input("input").value(), 1e-4);
		for (int i = 0; i < 50; i++) graphExecutor.step();
		assertEquals(0.003256475, (double) graphExecutor.graph().node("water").input("input").value(), 1e-4);
	}

	@SuppressWarnings("ConstantConditions")
	@Test
	public void should_parse_text_correctly_containing_operators() {
		String text = "" +
				"ExternalInput ei" + "\n" +
				"Output ei output Real\n" +
				"InitialValue ei.output 2\n" +
				"Offset offset Real 3\n" +
				"Input offset input Real\n" +
				"Output offset output Real\n" +
				"Gain gain Real 3\n" +
				"Input gain input Real\n" +
				"Output gain output Real\n" +
				"Adder adder Real\n" +
				"Input adder input1 Real\n" +
				"Input adder input2 Real\n" +
				"Output adder output Real\n" +
				"Multiplier multiplier Real\n" +
				"Input multiplier input1 Real\n" +
				"Input multiplier input2 Real\n" +
				"Output multiplier output Real\n" +
				"Connection ei.output offset.input\n" +
				"Connection ei.output gain.input\n" +
				"Connection offset.output adder.input1\n" +
				"Connection gain.output adder.input2\n" +
				"Connection offset.output multiplier.input1\n" +
				"Connection gain.output multiplier.input2\n" +
				"ConstantStepper 1\n" +
				"CoInit 100 1e-5\n" +
				"Simulation 0 1\n";
		Graph graph = DngFile.importDng(text).commit();
		assertEquals(5, graph.nodes().size());
		assertEquals(0, graph.node("ei").inputs().size());
		assertEquals(1, graph.node("ei").outputs().size());
		assertEquals(2.0, ((ExternalInput) graph.node("ei")).initialValues().get(0).value());
		assertEquals(Real, graph.variable("ei.output").type());
		assertEquals(1, graph.node("offset").inputs().size());
		assertEquals(1, graph.node("offset").outputs().size());
		assertEquals(3.0, ((Offset) graph.node("offset")).value());
		assertEquals(Real, graph.variable("offset.input").type());
		assertEquals(Real, graph.variable("offset.output").type());
		assertEquals(1, graph.node("gain").inputs().size());
		assertEquals(1, graph.node("gain").outputs().size());
		assertEquals(3.0, ((Gain) graph.node("gain")).value());
		assertEquals(Real, graph.variable("gain.input").type());
		assertEquals(Real, graph.variable("gain.output").type());
		assertEquals(2, graph.node("adder").inputs().size());
		assertEquals(1, graph.node("adder").outputs().size());
		assertEquals(Real, graph.variable("adder.input1").type());
		assertEquals(Real, graph.variable("adder.input2").type());
		assertEquals(Real, graph.variable("adder.output").type());
		assertEquals(2, graph.node("multiplier").inputs().size());
		assertEquals(1, graph.node("multiplier").outputs().size());
		assertEquals(Real, graph.variable("multiplier.input1").type());
		assertEquals(Real, graph.variable("multiplier.input2").type());
		assertEquals(Real, graph.variable("multiplier.output").type());
		assertEquals(6, graph.arrows().size());

		GraphExecutor graphExecutor = new GraphExecutor(graph);
		graphExecutor.execute();
		graphExecutor.logFile().delete();
		assertEquals(11.0, graph.variable("adder.output").value());
		assertEquals(30.0, graph.variable("multiplier.output").value());
	}

	@Test
	public void should_export_graph_correctly() {
		String text = "" +
				"FMU Tank \"" + fmuPath("TankGwin3264") + "\"\n" +
				"Output Tank outFlow Real\n" +
				"Input Tank Valve Boolean\n" +
				"FMU Barrel \"" + fmuPath("BarrelGwin3264") + "\"\n" +
				"Output Barrel Water Real\n" +
				"Input Barrel inFlow Real\n" +
				"Input Barrel Size Integer\n" +
				"Input Barrel Abort Boolean\n" +
				"FMU C1 \"" + fmuPath("C1win3264") + "\"\n" +
				"Output C1 Valve Boolean\n" +
				"FMU C2 \"" + fmuPath("C2win3264") + "\"\n" +
				"Output C2 Abort Boolean\n" +
				"Output C2 Size Integer\n" +
				"ExternalOutput water\n" +
				"Input water input Real\n" +
				"Connection C1.Valve Tank.Valve\n" +
				"Connection C2.Abort Barrel.Abort\n" +
				"Connection C2.Size Barrel.Size\n" +
				"Connection Tank.outFlow Barrel.inFlow\n" +
				"Connection Barrel.Water water.input\n" +
				"ConstantStepper 0.01\n" +
				"Simulation 0.0 18.0\n";

		eu.simulage.daccosim.view.Graph graph = Graphs.tankBarrelGraph();
		graph.settings().startTime(0).stopTime(18);
		graph.settings().stepper(ConstantStep).stepSize(0.01);
		assertEquals(text, new String(DngFile.exportDng(graph)));
	}

	@Test
	public void should_export_graph_correctly_with_operators() {
		String text = "" +
				"ExternalInput ei" + "\n" +
				"Output ei output Real\n" +
				"InitialValue ei.output 2.0\n" +
				"Offset offset Real 3.0\n" +
				"Output offset output Real\n" +
				"Input offset input Real\n" +
				"Gain gain Real 3.0\n" +
				"Output gain output Real\n" +
				"Input gain input Real\n" +
				"Adder adder Real\n" +
				"Output adder output Real\n" +
				"Input adder input1 Real\n" +
				"Input adder input2 Real\n" +
				"Multiplier multiplier Real\n" +
				"Output multiplier output Real\n" +
				"Input multiplier input1 Real\n" +
				"Input multiplier input2 Real\n" +
				"Connection ei.output offset.input\n" +
				"Connection ei.output gain.input\n" +
				"Connection offset.output adder.input1\n" +
				"Connection gain.output adder.input2\n" +
				"Connection offset.output multiplier.input1\n" +
				"Connection gain.output multiplier.input2\n" +
				"CoInit 100 1.0E-5\n" +
				"ConstantStepper 1.0\n" +
				"Simulation 0.0 1.0\n";

		eu.simulage.daccosim.view.Graph graph = Helper.graph()
				.nodes(
						externalInput("ei").outputs(output("output", Real)).initialValues(initialValue("output", 2.0)),
						offsetOperator("offset", 3.0).dataType(Real).inputs(input("input", Real)).output(output("output", Real)),
						gainOperator("gain", 3.0).dataType(Real).inputs(input("input", Real)).output(output("output", Real)),
						adderOperator("adder").dataType(Real).inputs(input("input1", Real), input("input2", Real)).output(output("output", Real)),
						multiplierOperator("multiplier").dataType(Real).inputs(input("input1", Real), input("input2", Real)).output(output("output", Real)))
				.arrows(
						arrow("ei.output", "offset.input"),
						arrow("ei.output", "gain.input"),
						arrow("offset.output", "adder.input1"),
						arrow("gain.output", "adder.input2"),
						arrow("offset.output", "multiplier.input1"),
						arrow("gain.output", "multiplier.input2"));
		graph.settings().startTime(0).stopTime(1);
		graph.settings().stepper(ConstantStep).stepSize(1);
		graph.settings().coInitialize();
		assertEquals(text, new String(DngFile.exportDng(graph)));
	}

	@Test
	public void should_provoke_an_exception_if_a_node_is_used_before_declaration() {
		String text = "" +
				"Output ei output Real\n" +
				"ExternalInput ei\n";
		try {
			DngFile.importDng(text).commit();
			fail();
		} catch (DaccosimException e) {
			assertEquals("ei node is not defined or it is used before its definition", e.getMessage());
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void should_provoke_an_exception_if_a_variable_is_used_before_declaration() {
		String text = "" +
				"ExternalInput ei\n" +
				"Connection ei.output offset.input\n" +
				"Output ei output Real\n";
		try {
			DngFile.importDng(text).commit();
			fail();
		} catch (DaccosimException e) {
			assertEquals("ei.output variable is not defined or it is used before its definition", e.getMessage());
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void should_provoke_an_exception_if_node_is_defined_twice() {
		String text = "" +
				"ExternalInput ei\n" +
				"ExternalInput ei\n" +
				"Output ei output Real\n";
		try {
			DngFile.importDng(text).commit();
			fail();
		} catch (DaccosimException e) {
			assertEquals("Node ei is already defined", e.getMessage());
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void should_provoke_an_exception_if_variable_is_defined_twice() {
		String text = "" +
				"ExternalInput ei\n" +
				"Output ei output Real\n" +
				"Output ei output Real\n";
		try {
			DngFile.importDng(text).commit();
			fail();
		} catch (DaccosimException e) {
			assertEquals("Variable ei.output is already defined", e.getMessage());
		} catch (Exception e) {
			fail();
		}
	}

	@Test
	public void should_provoke_an_exception_if_variable_is_logged_without_definition() {
		String text = "" +
				"Log ei.output\n";
		try {
			DngFile.importDng(text).commit();
			fail();
		} catch (DaccosimException e) {
			assertEquals("ei node is not defined or it is used before its definition", e.getMessage());
		} catch (Exception e) {
			fail();
		}
	}
}
