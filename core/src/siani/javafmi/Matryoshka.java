/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package siani.javafmi;

import eu.simulage.daccosim.DataType;
import eu.simulage.daccosim.GraphExecutor;
import eu.simulage.daccosim.model.*;
import eu.simulage.daccosim.model.nodes.ExternalInput;
import eu.simulage.daccosim.model.nodes.ExternalOutput;
import eu.simulage.daccosim.steppers.Constant;
import org.javafmi.framework.FmiSimulation;

import java.io.IOException;
import java.lang.Boolean;
import java.lang.Integer;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static eu.simulage.daccosim.DaccosimUtils.extract;
import static eu.simulage.daccosim.DataType.Boolean;
import static eu.simulage.daccosim.DataType.Integer;
import static eu.simulage.daccosim.DataType.*;
import static org.javafmi.framework.FmiContainer.Initial.calculated;
import static org.javafmi.framework.FmiContainer.Variability.continuous;
import static org.javafmi.framework.FmiContainer.Variability.discrete;

public class Matryoshka extends FmiSimulation {

	private Graph graph;
	private Map<DataType, VariableCreator> variableCreator;
	private GraphExecutor graphExecutor;
	private double stepSize = 0;

	public Matryoshka() {
	}

	private HashMap<DataType, VariableCreator> buildVariableCreator() {
		return new HashMap<DataType, VariableCreator>() {{
			put(Integer, (n, c, v) -> {
				IntegerVariable variable = variable(n).asInteger().causality(c).variability(discrete);
				if (v != null) variable.start((Integer) v);
				return variable;
			});
			put(Boolean, (n, c, v) -> {
				BooleanVariable variable = variable(n).asBoolean().causality(c).variability(discrete);
				if (v != null) variable.start((Boolean) v);
				return variable;
			});
			put(Real, (n, c, v) -> {
				RealVariable variable = variable(n).asReal().causality(c).variability(continuous).initial(calculated);
				if (v != null) variable.start((Double) v);
				return variable;
			});
			put(DataType.String, (n, c, v) -> {
				StringVariable variable = variable(n).asString().causality(c).variability(discrete);
				if (v != null) variable.start((String) v);
				return variable;
			});
		}};
	}

	private Graph graph() {
		if (variableCreator == null) variableCreator = buildVariableCreator();
		return graph == null ? graph = eu.simulage.daccosim.view.Graph.from(getJson()).commit() : graph;
	}

	private String getJson() {
		try {
			return new String(extract(Matryoshka.class.getResourceAsStream("/model.dsg")));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public Model define() {
		Model model = model(Matryoshka.class.getSimpleName());
		model.description("Daccosim NG Matryoshka FMU");
		graph().nodes().forEach(n -> {
			if (n instanceof ExternalInput) n.outputs().forEach(o -> addInput(model, o));
			else if (n instanceof ExternalOutput) n.inputs().forEach(i -> addOutput(model, i));
			else allVariables(n).forEach(v -> addVariable(model, v));
		});
		model.canGetAndSetFMUstate(false);
		model.canSerializeFMUstate(false);
		model.providesDirectionalDerivative(false);
		model.canBeInstantiatedOnlyOncePerProcess(true);
		model.canHandleVariableCommunicationStepSize(true);
		return model;
	}

	private void addInput(Model model, Output output) {
		model.add(variableCreator.get(output.type()).create(output.path(), Causality.input, output.value()));
	}

	private void addOutput(Model model, Input input) {
		model.add(variableCreator.get(input.type()).create(input.path(), Causality.output, null));
	}

	private void addVariable(Model model, Variable variable) {
		model.add(variableCreator.get(variable.type()).create(variable.path(), Causality.local, null));
	}

	private List<Variable> allVariables(GraphNode n) {
		List<Variable> result = new ArrayList<>(n.inputs());
		result.addAll(n.variables());
		result.addAll(n.outputs());
		return result;
	}

	@Override
	public Status init() {
		graph().nodes().forEach(n -> allVariables(n).forEach(this::register));
		graphExecutor = new GraphExecutor(graph(), new Constant(graph()) {
			@Override
			protected double stepSize() {
				return stepSize;
			}
		});
		graphExecutor.isLogInFile(false);
		graphExecutor.load();
		graphExecutor.init();
		return Status.OK;
	}

	private void register(Variable variable) {
		register(variable.path(), new org.javafmi.framework.Variable() {
			@Override
			public Object getValue() {
				return variable.value();
			}

			@Override
			public Status setValue(Object o) {
				variable.value(o);
				return Status.OK;
			}
		});
	}

	@Override
	public Status doStep(double v) {
		stepSize = v;
		if (graphExecutor.time() == 0) graphExecutor.firstStep();
		else graphExecutor.step();
		return Status.OK;
	}

	@Override
	public Status reset() {
		return Status.OK;
	}

	@Override
	public Status terminate() {
		graphExecutor.terminate();
		return Status.OK;
	}

	private interface VariableCreator {
		ScalarVariable create(String name, Causality causality, Object value);
	}

}
