/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim;

import Jama.Matrix;
import eu.simulage.daccosim.model.Arrow;
import eu.simulage.daccosim.model.FMUProxy;
import eu.simulage.daccosim.model.Graph;
import eu.simulage.daccosim.model.GraphNode;

import java.util.*;

import static eu.simulage.daccosim.GraphExecutor.logger;
import static eu.simulage.daccosim.Iterator.streamOf;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

class CoInitializer {

	private final Graph graph;
	private final List<Arrow> arrows;

	private CoInitializer(Graph graph) {
		this.graph = graph;
		this.arrows = graph.arrows().stream().filter(a -> a.from().type().equals(DataType.Real)).collect(toList());
	}

	static void coInitialize(Graph graph) {
		new CoInitializer(graph).coInitialize();
	}

	private void coInitialize() {// TODO sort loops according to flow
		List<List<Arrow>> loops = loopedArrows();
		transmitOutputs(loops);
		logger().info("Number of SCC: " + loops.size());
		ArrayList<List<Arrow>> clone = new ArrayList<>(loops);
		for (int i = 0, listsSize = clone.size(); i < listsSize; i++) {
			logger().info("SSC_" + (i+1) + " had " + coInitialize(clone.get(i)) + " iterations");
			transmitOutputs(loops);
		}
	}

	private int coInitialize(List<Arrow> loop) {
		int iteration = 0;
		for (; iteration < graph.settings().coInitialization().maxIterations(); iteration++) {
			Matrix residuals = residuals(loop);
			if (residuals.normInf() < graph.settings().coInitialization().residualsTolerance()) break;
			List<Double> corrections = calculateCorrections(loop, residuals);
			range(0, loop.size()).forEach(i -> loop.get(i).to().value((double) loop.get(i).to().value() + corrections.get(i)));
		}
		return iteration;
	}

	private void transmitOutputs(List<List<Arrow>> excludedArrows) {
		List<Arrow> arrows = new ArrayList<>(this.arrows);
		excludedArrows.forEach(arrows::removeAll);
		List<Object> fromValues = streamOf(arrows).map(a -> a.from().value()).collect(toList());
		List<Object> newFromValues = fromValues;
		do {
			List<Object> finalFromValues = fromValues = newFromValues;
			range(0, fromValues.size()).forEach(i -> arrows.get(i).to().value(finalFromValues.get(i))); // TODO parallel or not
			newFromValues = streamOf(arrows).map(a -> a.from().value()).collect(toList());
		} while (areDifferent(fromValues, newFromValues));
	}

	private boolean areDifferent(List<Object> fromValues, List<Object> newFromValues) {
		for (int i = 0; i < fromValues.size(); i++) if (!fromValues.get(i).equals(newFromValues.get(i))) return true;
		return false;
	}

	private Matrix residuals(List<Arrow> arrows) {
		Matrix residuals = new Matrix(arrows.size(), 1);
		range(0, arrows.size()).forEach(i -> residuals.set(i, 0, -((double) arrows.get(i).to().value() - (double) arrows.get(i).from().value()))); // TODO
		return residuals;
	}

	private List<Double> calculateCorrections(List<Arrow> loop, Matrix residuals) { // TODO protect nodes are FMU
		Matrix correction = jacobian(loop).solve(residuals);
		List<Double> corrections = new ArrayList<>();
		for (int i = 0; i < correction.getRowDimension(); i++) corrections.add(correction.get(i, 0));
		return corrections;
	}

	private Matrix jacobian(List<Arrow> loop) {
		Matrix jacobian = new Matrix(loop.size(), loop.size());
		// TODO -> keep for the moment as derivatives are cached
		range(0, loop.size()).forEach(i -> range(0, loop.size())
				.forEach(j -> jacobian.set(j, i, i != j ? ((FMUProxy) loop.get(i).toNode()).derivativeOf(loop.get(i).to().id(), loop.get(j).from().id()) : 1.0)));
		return jacobian;
	}

	private List<List<Arrow>> loopedArrows() {
		List<List<Arrow>> result = new ArrayList<>();
		List<Arrow> arrows = this.arrows.stream().filter(this::isCyclic).collect(toList());
		while (!arrows.isEmpty()) {
			List<Arrow> independentArrows = independentArrows(arrows);
			result.add(independentArrows);
			arrows.removeAll(independentArrows);
		}
		return result;
	}

	private List<Arrow> independentArrows(List<Arrow> arrows) {
		if (arrows.size() < 2) return arrows;
		Set<Arrow> result = new LinkedHashSet<>();
		result.add(arrows.get(0));
		Set<GraphNode> participants = new HashSet<>(asList(arrows.get(0).fromNode(), arrows.get(0).toNode()));
		int size = 0;
		while (participants.size() != size) {
			size = participants.size();
			range(1, arrows.size()).forEach(i -> {
				if (!participants.contains(arrows.get(i).toNode()) && !participants.contains(arrows.get(i).fromNode()))
					return;
				participants.add(arrows.get(i).fromNode());
				participants.add(arrows.get(i).toNode());
				result.add(arrows.get(i));
			});
		}
		return new ArrayList<>(result);
	}

	private boolean isCyclic(Arrow arrow) {
		return arrows.stream()
				.filter(a -> !a.equals(arrow))
				.anyMatch(a -> a.fromNode().equals(arrow.toNode()) && a.toNode().equals(arrow.fromNode()));
	}

}
