/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim.steppers;

import eu.simulage.daccosim.Stepper;
import eu.simulage.daccosim.model.FMUProxy;
import eu.simulage.daccosim.model.Graph;
import eu.simulage.daccosim.model.Variable;
import eu.simulage.daccosim.model.outputs.FMUOutput;

import java.util.*;

import static eu.simulage.daccosim.Iterator.streamOf;
import static java.util.stream.Collectors.toList;

public abstract class DynamicStepper extends Stepper {

	double stepSize;
	final Map<Variable, Double> previousValues = new HashMap<>();
	final Map<Variable, Double> previousDerivatives = new HashMap<>();
	private int rollbacksCount = 0;
	private List<FMUProxy> fmuNodes = new ArrayList<>();
	List<StepperVariable> stepperVariables = new ArrayList<>();

	DynamicStepper(Graph graph) {
		super(graph);
	}

	@Override
	public void init() {
		fmuNodes = graph.nodes().stream().filter(n -> n instanceof FMUProxy).map(n -> (FMUProxy) n).collect(toList());
		stepperVariables = graph.settings().stepper().stepperVariables().entrySet().stream()
				.map(v -> {
					Variable variable = graph.variable(v.getKey());
					if (!(variable instanceof FMUOutput)) return null;
					return new StepperVariable(variable, v.getValue());
				})
				.filter(Objects::nonNull)
				.collect(toList());
		stepSize = graph.settings().stepper().stepSize();
	}


	private void saveState() {
		previousValues.clear();
		previousDerivatives.clear();
		streamOf(fmuNodes).forEach(FMUProxy::saveState);
		streamOf(stepperVariables).forEach(stepperVariable -> {
			previousValues.put(stepperVariable.variable, (Double) stepperVariable.variable.value());
			previousDerivatives.put(stepperVariable.variable, stepperVariable.variable.derivative());
		});
	}

	private void performStep(double stepSize) {
		streamOf(graph.nodes()).forEach(n -> n.step(stepSize));
	}

	@Override
	public double firstStep() {
		performStep(stepSize);
		exchangeOutputs();
		time += stepSize;
		return stepSize;
	}

	private double doStep() {
		double stepSize = this.stepSize;
		saveState();
		double suggestedStepSize = doStep(stepSize);
		while (suggestedStepSize < stepSize) {
			rollbacksCount++;
			rollback();
			this.stepSize = stepSize = suggestedStepSize;
			suggestedStepSize = doStep(stepSize);
		}
		this.stepSize = suggestedStepSize >= minStep() ? suggestedStepSize : this.stepSize;
		time += stepSize;
		exchangeOutputs();
		return stepSize;
	}

	private double doStep(double stepSize) {
		performStep(stepSize);
		double suggestedStep = suggestedStep(stepSize);
		return suggestedStep < minStep() ? minStep() : suggestedStep;
	}

	@Override
	public double step() {
		exchangeVars();
		return doStep();
	}

	@Override
	public int rollbacksCount() {
		return rollbacksCount;
	}

	abstract double suggestedStep(double stepSize);

	void rollback() {
		streamOf(fmuNodes).forEach(FMUProxy::restoreState);
	}

	double maxStep() {
		return graph.settings().stopTime() - time();
	}

	static class StepperVariable {

		final Variable variable;
		final double tolerance;

		StepperVariable(Variable variable, double tolerance) {
			this.variable = variable;
			this.tolerance = tolerance;
		}
	}
}
