/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim.steppers;

import eu.simulage.daccosim.Stepper;
import eu.simulage.daccosim.model.Graph;

import static eu.simulage.daccosim.Iterator.streamOf;

public class Constant extends Stepper {

	public Constant(Graph graph) {
		super(graph);
	}

	@Override
	public void init() {
	}

	@Override
	public double firstStep() {
//        exchangeOutputs();
		double stepSize = doStep();
		exchangeVars();
		return stepSize;
	}

	@Override
	public double step() {
		// TODO FMU
//        exchangeVars();
		doStep();
		exchangeVars();
		return stepSize();
	}

	@Override
	public int rollbacksCount() {
		return 0;
	}

	private double doStep() {
		streamOf(graph.nodes()).forEach(node -> node.step(stepSize()));
		time += stepSize();
		return stepSize();
	}
}
