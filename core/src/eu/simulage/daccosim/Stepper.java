/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim;

import eu.simulage.daccosim.model.Arrow;
import eu.simulage.daccosim.model.Graph;
import eu.simulage.daccosim.model.outputs.FMUOutput;

import java.util.List;

import static eu.simulage.daccosim.Iterator.streamOf;
import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;

public abstract class Stepper {

    protected final Graph graph;
    private final List<Arrow> nonFmuArrows;
    protected double time = 0;

    protected Stepper(Graph graph) {
        this.graph = graph;
        this.nonFmuArrows = graph.arrows().stream().filter(a -> !(a.from() instanceof FMUOutput)).collect(toList());
    }

    protected void exchangeVars() {
        exchangeValues(valuesToExchange());
    }

    private void exchangeValues(List<Object> values) {
        range(0, values.size()).forEach(i -> graph.arrows().get(i).to().value(values.get(i))); // TODO
    }

    private List<Object> valuesToExchange() {
        return streamOf(graph.arrows()).map(a -> a.from().value()).collect(toList());
    }

    protected void exchangeOutputs() {
        List<Object> values = streamOf(nonFmuArrows).map(a -> a.from().value()).collect(toList());
        range(0, values.size()).parallel().forEach(i -> nonFmuArrows.get(i).to().value(values.get(i)));
	}

    public abstract void init();

    public abstract double firstStep();

    public abstract double step();

    public abstract int rollbacksCount();

    protected double stepSize() {
        return graph.settings().stepper().stepSize();
    }

    protected double safetyFactor() {
        return graph.settings().stepper().safetyFactor();
    }

    protected double minStep() {
        return graph.settings().stepper().minStep();
    }

    protected int order() {
        return graph.settings().stepper().order();
    }

    public double time() {
        return time;
    }

    public enum Method {
        ConstantStep, Euler, AdamsBashforth
    }

}
