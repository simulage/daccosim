/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim;

import eu.simulage.daccosim.model.Graph;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static eu.simulage.daccosim.Stepper.Method.AdamsBashforth;
import static eu.simulage.daccosim.Stepper.Method.ConstantStep;

public class DaccosimUtils {

	static String printParameters(Graph graph, File outputFile) {
		StringBuilder builder = new StringBuilder();
		writeVar(builder, "Start time", graph.settings().startTime());
		writeVar(builder, "Stop time", graph.settings().stopTime());
		writeVar(builder, "Co-init", graph.settings().isCoInitialized() ? "enabled" : "disabled");
		if (graph.settings().isCoInitialized()) {
			writeVar(builder, "CoInit max iterations", graph.settings().coInitialization().maxIterations());
			writeVar(builder, "CoInit tolerance", graph.settings().coInitialization().residualsTolerance());
		}
		Graph.Settings.Stepper stepper = graph.settings().stepper();
		Stepper.Method method = stepper.method();
		writeVar(builder, "Stepper method", method);
		if (method.equals(ConstantStep)) writeVar(builder, "stepSize", stepper.stepSize());
		if (!method.equals(ConstantStep)) writeVar(builder, "minStep", stepper.minStep());
		if (!method.equals(ConstantStep)) writeVar(builder, "safetyFactor", stepper.safetyFactor());
		if (method.equals(AdamsBashforth)) writeVar(builder, "order", stepper.order());
		if (!method.equals(ConstantStep)) writeVar(builder, "maxNumberOfSteps", stepper.maxNumberOfSteps());
		if (!method.equals(ConstantStep)) {
			Double aDouble = stepper.stepperVariables().values().iterator().next();
			if (stepper.stepperVariables().values().stream().allMatch(v -> v.equals(aDouble)))
				writeVar(builder, "stepperTolerance", aDouble);
		}
		if (graph.export() != null) {
			writeVar(builder, "CSV cell separator", graph.export().cellSeparator());
			writeVar(builder, "CSV decimal separator", graph.export().decimalSeparator());
			writeVar(builder, "Output file", outputFile.getAbsolutePath());
		}
		return builder.toString();
	}

	private static void writeVar(StringBuilder builder, String varName, Object value) {
		builder.append("\n\t").append(varName).append(": ").append(value);
	}

	public static byte[] extract(InputStream inputStream) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int read;
		while ((read = inputStream.read(buffer, 0, buffer.length)) != -1) {
			baos.write(buffer, 0, read);
		}
		baos.flush();
		return baos.toByteArray();
	}

}
