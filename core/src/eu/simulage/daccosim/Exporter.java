/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim;

import eu.simulage.daccosim.model.Export;
import eu.simulage.daccosim.model.Graph;
import eu.simulage.daccosim.model.GraphNode;
import eu.simulage.daccosim.model.Variable;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

class Exporter {

	private final List<Variable> variableList;
	private final File file;
	private final BufferedWriter writer;
	private final Export export;
	private double time = 0;

	Exporter(Graph graph) {
		try {
			this.export = graph.export();
			this.variableList = !isExportNull() ? variablesOf(graph) : null;
			this.file = !isExportNull() ? new File(folder(), filename()) : null;
			this.writer = file != null ? new BufferedWriter(new FileWriter(file)) : null;
			writeHeaders();
		} catch (IOException e) {
			throw new DaccosimException(e.getMessage());
		}
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	private String folder() {
		String path = export.folder();
		new File(path).mkdirs();
		return path;
	}

	@SuppressWarnings("SpellCheckingInspection")
	private String filename() {
		return (export.prefix().isEmpty() ? "" : export.prefix() + "-") +
				LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + ".csv";
	}

	private boolean isExportNull() {
		return export == null || export.variables().isEmpty();
	}

	private void writeHeaders() {
		if (isExportNull()) return;
		StringBuilder builder = new StringBuilder();
		builder.append("time").append(export.cellSeparator()).append("dt");
		variableList.forEach(c -> builder.append(export.cellSeparator()).append(c.path()));
		write(builder.append("\n").toString());
	}

	private void write(String string) {
		try {
			writer.write(string);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private List<Variable> variablesOf(Graph graph) {
		return isExportAll(graph.export()) ? allVariables(graph) : variablesOf(graph.export().variables(), graph);
	}

	private List<Variable> allVariables(Graph graph) {
		return graph.nodes().stream().map(this::variables).flatMap(Collection::stream).collect(toList());
	}

	private List<Variable> variablesOf(List<String> variables, Graph graph) {
		List<Variable> result = new ArrayList<>();
		variables.forEach(v -> result.add(graph.variable(v)));
		return result;
	}

	private List<Variable> variables(GraphNode node) {
		List<Variable> result = new ArrayList<>(node.inputs());
		result.addAll(node.outputs());
		result.addAll(node.variables());
		return result;
	}

	private boolean isExportAll(Export export) {
		return !export.variables().isEmpty() && export.variables().get(0).equals("all");
	}

	void export(double time) {
		if (isExportNull()) return;
		StringBuilder builder = new StringBuilder();
		builder.append(format(time)).append(export.cellSeparator()).append(format(time - this.time));
		variableList.stream().map(Variable::value).collect(toList())
				.forEach(c -> builder.append(export.cellSeparator()).append(format(c)));
		write(builder.append("\n").toString());
		this.time = time;
	}

	private String format(Object value) {
		return value == null ? "null" :
				value instanceof Double ? value.toString().replace(".", export.decimalSeparator()) :
						value instanceof Boolean ? value.equals(true) ? "1" : "0" :
								value.toString();
	}

	void terminate() {
		if (isExportNull()) return;
		try {
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	File file() {
		return file;
	}
}
