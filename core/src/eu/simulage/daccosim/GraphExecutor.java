/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim;

import eu.simulage.daccosim.model.Graph;
import eu.simulage.daccosim.model.GraphNode;
import eu.simulage.daccosim.steppers.AdamsBashforth;
import eu.simulage.daccosim.steppers.Constant;
import eu.simulage.daccosim.steppers.DynamicStepper;
import eu.simulage.daccosim.steppers.Euler;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.*;

import static eu.simulage.daccosim.Stepper.Method.ConstantStep;
import static eu.simulage.daccosim.Stepper.Method.Euler;
import static java.lang.System.currentTimeMillis;
import static java.util.concurrent.Executors.newScheduledThreadPool;

public class GraphExecutor {

	private static final String DACCOSIM_LOGGER = "daccosim";
    private static final Logger logger;

	static {
		System.setProperty("java.util.logging.SimpleFormatter.format", "[%1$tF %1$tT] [%4$s] %5$s %n");
		Locale.setDefault(new Locale("en", "EN"));
		logger = Logger.getLogger(DACCOSIM_LOGGER);
		logger.setUseParentHandlers(false);
		logger.addHandler(new StreamHandler(System.out, new SimpleFormatter()){
			@Override
			public synchronized void publish(LogRecord record) {
				super.publish(record);
				flush();
			}
		});
	}

	private final Graph graph;
	private final Stepper stepper;
	private Exporter exporter;
	private double time;
	private int numberOfSteps = 0;
	private int maxNumberOfSteps;
	private double stopTime;
	private boolean isLogInFile = true;
	private ScheduledExecutorService service;
	private FileHandler logHandler;

	public GraphExecutor(Graph graph) {
		this(graph, null);
	}

	public GraphExecutor(Graph graph, Stepper stepper) {
		this.graph = graph;
		this.stepper = stepper == null ? buildStepper() : stepper;
		this.exporter = new Exporter(graph);
		Iterator.configure(graph);
	}

	public static void execute(Graph graph) {
		new GraphExecutor(graph).execute();
	}

	public static Logger logger() {
		return logger;
	}

	public void execute() {
		if (graph.settings().jmsDataConnection() != null && !graph.settings().jmsDataConnection().isCoordinator())
			return;
		if (isLogInFile) {
			logHandler = logFileHandler();
			if(logHandler != null) logger().addHandler(logHandler);
		}
		logger().info("Co-sim started" + DaccosimUtils.printParameters(graph, exportFile()));
		service = newScheduledThreadPool(1);
		service.scheduleAtFixedRate(() -> logger().info("Simulation time: " + time()), 30, 30, TimeUnit.SECONDS);
		long millis = currentTimeMillis();
		load();
		init();
		if (time < stopTime) coSimulate();
		terminate();
		logger().info("Co-sim finished with " + numberOfSteps + " steps. Total time (load + co-init + co-sim): " + (currentTimeMillis() - millis) + " ms" + (stepper instanceof DynamicStepper ? ". Rollbacks: " + stepper.rollbacksCount() : ""));
	}

	private FileHandler logFileHandler() {
		try {
			String pattern = exporter.file() == null ? "output.log" : exporter.file().getAbsolutePath().replace(".csv", ".log");
			FileHandler fileHandler = new FileHandler(pattern);
			fileHandler.setFormatter(new SimpleFormatter());
			return fileHandler;
		} catch (IOException e) {
			logger().severe(e.getMessage());
			return null;
		}
	}

	private void coSimulate() {
		time += firstStep();
		exporter.export(time);
		while (time < stopTime && numberOfSteps < maxNumberOfSteps) {
			time += step();
			exporter.export(time);
		}
	}

	public void terminate() {
		graph.nodes().forEach(GraphNode::terminate);
		exporter.terminate();
		logHandler.close();
		if (service != null) service.shutdownNow();
	}

	public void load() {
		graph.nodes().forEach(GraphNode::load);
	}

	public void init() {
		graph.nodes().forEach(GraphNode::init);
		if (graph.settings().isCoInitialized()) coInit();
		stepper.init();
		exporter.export(graph.settings().startTime());
		stopTime = graph.settings().stopTime();
		time = graph.settings().startTime();
		maxNumberOfSteps = graph.settings().stepper().maxNumberOfSteps();
	}

	private void coInit() {
		long millis = currentTimeMillis();
		CoInitializer.coInitialize(graph);
		logger().info("Co-init elapsed time " + (currentTimeMillis() - millis) + " ms");
	}

	public double firstStep() {
		numberOfSteps++;
		return stepper.firstStep();
	}

	public double step() {
		numberOfSteps++;
		return stepper.step();
	}

	private Stepper buildStepper() {
		return graph.settings().stepper().method() == ConstantStep ?
				new Constant(graph) : graph.settings().stepper().method() == Euler ? new Euler(graph) : new AdamsBashforth(graph);
	}

	public Graph graph() {
		return graph;
	}

	public double time() {
		return stepper.time();
	}

	File exportFile() {
		return exporter.file();
	}

	public void isLogInFile(boolean value) {
		isLogInFile = value;
	}

	File logFile() {
		return exportFile() != null ? new File(exportFile().getAbsolutePath().replace(".csv", ".log")) : new File("output.log");
	}
}
