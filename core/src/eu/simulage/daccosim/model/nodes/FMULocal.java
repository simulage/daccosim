/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim.model.nodes;

import eu.simulage.daccosim.DataType;
import eu.simulage.daccosim.model.*;
import eu.simulage.daccosim.model.inputs.FMUInput;
import eu.simulage.daccosim.model.outputs.FMUOutput;
import eu.simulage.daccosim.model.variables.FMUVariable;
import org.javafmi.modeldescription.v2.ModelDescription;
import org.javafmi.proxy.v2.State;
import org.javafmi.wrapper.Simulation;
import org.javafmi.wrapper.v2.Access;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.javafmi.proxy.v2.State.newEmptyState;

public class FMULocal extends GraphNode implements FMUProxy {

	private static final Map<String, SimulationPuller> puller = new HashMap<String, SimulationPuller>() {{
		put(DataType.Real.toString(), (sim, id) -> sim.read(id).asDouble());
		put(DataType.Integer.toString(), (sim, id) -> sim.read(id).asInteger());
		put(DataType.Boolean.toString(), (sim, id) -> sim.read(id).asBoolean());
		put(DataType.String.toString(), (sim, id) -> sim.read(id).asString());
		put(DataType.Enumeration.toString(), (sim, id) -> sim.read(id).asInteger());
	}};
	private static final Map<String, SimulationPusher> pusher = new HashMap<String, SimulationPusher>() {{
		put(DataType.Real.toString(), (sim, id, value) -> sim.write(id).with((double) value));
		put(DataType.Integer.toString(), (sim, id, value) -> sim.write(id).with((int) value));
		put(DataType.Boolean.toString(), (sim, id, value) -> sim.write(id).with((boolean) value));
		put(DataType.String.toString(), (sim, id, value) -> sim.write(id).with((String) value));
		put(DataType.Enumeration.toString(), (sim, id, value) -> sim.write(id).with((Integer) value));
	}};

	private final String path;
	private Simulation simulation;
	private ModelDescription modelDescription;
	private Access access;
	private final Map<String, Double> derivativesMap = new HashMap<>();
	private final Map<String, Object> variablesMap = new HashMap<>();
	private double currentTime = 0;
	private State state = newEmptyState();
	private double stateTime;
	private boolean inInit = false;
	private List<InitialValue> beforeInitValues;
	private List<InitialValue> inInitValues;

	public FMULocal(String id, String path) {
		super(id);
		this.path = path;
	}

	String variableType(String id) {
		return simulation.getModelDescription().getModelVariable(id).getTypeName();
	}

	@Override
	public void load() {
		this.simulation = new Simulation(path);
		this.modelDescription = (ModelDescription) simulation.getModelDescription();
		this.access = new Access(simulation);
	}

	@Override
	public void init() {
		inInit = true;
		access.setUpExperiment(true, modelDescription.getDefaultExperiment().getTolerance(), graph().settings().startTime(), true, graph().settings().stopTime());
		beforeInitValues.forEach(b -> {
			Input input = input(b.name());
			if (input != null) input.value(b.value());
			else variable(b.name()).value(b.value());
		});
		access.enterInitializationMode();
		stream(simulation.getModelDescription().getModelVariables())
				.filter(v -> v.getCausality().equals("input") && v.getStart() != null)
				.forEach(v -> input(v.getName()).value(v.getStart()));
		inInitValues.forEach(b -> input(b.name()).value(b.value()));
		access.exitInitializationMode();
		currentTime = graph().settings().startTime();
	}

	@Override
	public void step(double stepSize) {
		inInit = false;
		access.doStep(currentTime, stepSize);
		currentTime += stepSize;
		clearCache();
		variables().forEach(Variable::value);
		outputs().forEach(Variable::value);
	}

	@Override
	public void terminate() {
		simulation.terminate();
	}

	public FMULocal inputs(Input... inputs) {
		super.inputs(stream(inputs).map(i -> new FMUInput(i.id(), i.type(), this)).collect(toList()));
		return this;
	}

	public FMULocal outputs(Output... outputs) {
		super.outputs(stream(outputs).map(i -> new FMUOutput(i.id(), i.type(), this)).collect(toList()));
		return this;
	}

	public FMULocal variables(Variable... variable) {
		super.variables(stream(variable).map(i -> new FMUVariable(i.id(), i.type(), this)).collect(toList()));
		return this;
	}

	@Override
	public double derivativeOf(String input, String output) {
		if (input.equals(output)) return 0;
		String id = input + output;
		if (derivativesMap.containsKey(id)) return derivativesMap.get(id);
		double value = doDerivativeOf(input, output);
		derivativesMap.put(id, value);
		return value;
	}

	private double doDerivativeOf(String input, String output) {
		int outputReference = valueReference(output);
		int inputReference = valueReference(input);
		if (outputReference == -1 || inputReference == -1) return 0;
		return -new Access(simulation).getDirectionalDerivative(
				new int[]{outputReference},
				new int[]{inputReference},
				new double[]{1.0})[0];
	}

	@Override
	public double derivativeOf(String output) {
		if (derivativesMap.containsKey(output)) return derivativesMap.get(output);
		double value = access.getRealOutputDerivatives(new String[]{output})[0];
		derivativesMap.put(output, value);
		return value;
	}

	private int valueReference(String varName) {
		try {
			return simulation.getModelDescription().getValueReference(varName);
		} catch (Exception e) {
			return -1;
		}
	}

	@Override
	public void saveState() {
		state = access.getState(state);
		stateTime = currentTime;
	}

	@Override
	public void restoreState() {
		clearCache();
		access.setState(state);
		currentTime = stateTime;
	}

	private void clearCache() {
		derivativesMap.clear();
		variablesMap.clear();
	}

	@Override
	public void push(String id, Object value) {
		if (inInit) clearCache();
		pusher.get(variableType(id)).push(simulation, id, value);
		variablesMap.put(id, value);
	}

	@Override
	public Object pull(String id) {
		if (variablesMap.containsKey(id)) return variablesMap.get(id);
		Object value = puller.get(variableType(id)).pull(simulation, id);
		variablesMap.put(id, value);
		return value;
	}

	public FMULocal beforeInitValues(List<InitialValue> initialValues) {
		this.beforeInitValues = initialValues;
		return this;
	}


	public FMULocal inInitValues(List<InitialValue> initialValues) {
		this.inInitValues = initialValues;
		return this;
	}

	Simulation simulation() {
		return simulation;
	}

	interface SimulationPuller {
		Object pull(Simulation simulation, String id);
	}

	private interface SimulationPusher {
		void push(Simulation simulation, String id, Object value);
	}
}
