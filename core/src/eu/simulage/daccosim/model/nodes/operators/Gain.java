/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.daccosim.model.nodes.operators;

import eu.simulage.daccosim.DataType;
import eu.simulage.daccosim.model.nodes.Operator;

public class Gain extends Operator {

	private Object value;
	private Calculator calculator = () -> (double) inputs().get(0).value() * (double) value;

	public Gain(String id) {
		super(id);
	}

	@Override
	public void init() {
		super.init();
		if (dataType.equals(DataType.Integer)) calculator = () -> (int) inputs().get(0).value() * (int) value;
		if (dataType.equals(DataType.Boolean)) calculator = () -> (boolean) inputs().get(0).value() && (boolean) value;
	}

	@Override
	public void step(double stepSize) {
		outputs().get(0).value(calculator.calculate());
	}

	public Gain value(Object value) {
		this.value = value;
		return this;
	}

	public Object value() {
		return this.value;
	}

}
