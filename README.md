DACCOSIM NG is a collaborative development effort between EDF Lab Paris-Saclay (France),
CentraleSupélec (France), EIFER - European Institute for Energy Research (Germany), SIANI institute (Spain),
and Monentia S.L. (Spain).

This File is part of DACCOSIM Project.

DACCOSIM NG is a free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License.

DACCOSIM Project is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Daccosim. If not, see <http://www.gnu.org/licenses>.