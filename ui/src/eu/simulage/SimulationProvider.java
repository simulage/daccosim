/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage;

import eu.simulage.daccosim.DaccosimException;
import org.javafmi.kernel.OS;
import org.javafmi.wrapper.Simulation;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipFile;

public class SimulationProvider {
	public static Map<String, Simulation> simulationMap = new HashMap<>();

	public static Simulation getSimulation(File selectedFile) {
		try {
			return doGetSimulation(selectedFile);
		} catch (Exception e) {
			if (e instanceof DaccosimException) throw e;
			return null;
		}
	}

	public static Simulation unsafeGetSimulation(File selectedFile) {
		return doGetSimulation(selectedFile);
	}

	private static Simulation doGetSimulation(File selectedFile) {
		if (!selectedFile.exists() || !selectedFile.getName().endsWith(".fmu")) return null;
		if (!simulationMap.containsKey(selectedFile.getAbsolutePath()))
			simulationMap.put(selectedFile.getAbsolutePath(), simulationOf(selectedFile));
		return simulationMap.get(selectedFile.getAbsolutePath());
	}

	private static Simulation simulationOf(File selectedFile) {
		if (!isSystemCompatible(selectedFile))
			throw new DaccosimException("Invalid FMU: " + selectedFile.getName() + "\nBinaries for " + osSystem() + OS.architecture() + " not found");
		try {
			Simulation simulation = new Simulation(selectedFile.getAbsolutePath());
			simulation.init(0);
			simulation.terminate();
			return simulation;
		} catch (Exception e) {
			e.printStackTrace(System.out);
			throw e;
		}
	}

	private static boolean isSystemCompatible(File selectedFile) {
		try (ZipFile file = new ZipFile(selectedFile)) {
			String folderToFind = "binaries/" + osSystem() + OS.architecture();
			return file.stream().anyMatch(e -> e.getName().startsWith(folderToFind));
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	private static String osSystem() {
		return OS.name().equalsIgnoreCase("windows") ? "win" : OS.name();
	}

	public static void removeSimulation(File file) {
		simulationMap.remove(file.getAbsolutePath());
	}
}
