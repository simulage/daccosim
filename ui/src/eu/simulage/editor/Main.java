/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor;

import com.mxgraph.io.mxCodecRegistry;
import com.mxgraph.io.mxObjectCodec;
import eu.simulage.editor.specs.*;
import eu.simulage.editor.views.AboutDialog;

import javax.swing.*;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.net.URL;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import static eu.simulage.daccosim.GraphExecutor.logger;

public class Main extends Editor {

	private File logFile = new File("editor.log");

	private Main() throws FileNotFoundException {
		super(new GraphComponent(new Graph()));
		EditorUtils.fillWith(graphComponent);
		addCodecsForSerialization();
		logFile.delete();
		System.setOut(new PrintStream(logFile));
		System.setErr(new PrintStream(logFile));
	}

//	public static void main(String[] args) {
//		try {
//			MessageDigest digest = MessageDigest.getInstance("MD5");
//			digest.update(Files.readAllBytes(new File("D:\\Users\\jevora\\repositories\\daccosim-simulage\\user-guide-use-cases\\3-industrial\\3-iDEAS\\RealExpression50win3264.fmu").toPath()));
//			System.out.println(DatatypeConverter.printHexBinary(digest.digest()).toUpperCase());
//		} catch (NoSuchAlgorithmException | IOException e) {
//			logger().severe(e.getMessage());
//		}
//	}

	public static void main(String[] args) throws FileNotFoundException {
		splashScreen();
		new Main();
	}

	private static void splashScreen() {
		JWindow window = new JWindow();
		window.getContentPane().add(new AboutDialog());
		window.setSize(500, 500);
		window.setLocationRelativeTo(null);
		window.setVisible(true);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		window.setVisible(false);
	}

	public static URL imageOf(String name) {
		return Main.class.getResource("/images/" + name);
	}

	private void addCodecsForSerialization() {
		mxCodecRegistry.addPackage("eu.simulage.daccosim.specs");
		mxCodecRegistry.register(new mxObjectCodec(new FmuSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new AdderSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new OffsetSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new MultiplierSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new GainSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new ArrowSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new ConnectionSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new ExternalOutputSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new VariableSpec()));
		mxCodecRegistry.register(new mxObjectCodec(new ExternalInputSpec()));
	}


}
