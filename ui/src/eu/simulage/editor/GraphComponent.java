/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor;

import com.mxgraph.io.mxCodec;
import com.mxgraph.model.mxICell;
import com.mxgraph.model.mxIGraphModel;
import com.mxgraph.swing.handler.mxSelectionCellsHandler;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxGraph;
import eu.simulage.editor.specs.*;
import org.w3c.dom.Document;

import javax.swing.*;
import java.awt.*;

public class GraphComponent extends mxGraphComponent {

	GraphComponent(mxGraph graph) {
		super(graph);
		setToolTips(true);
		setGridVisible(true);
		getConnectionHandler().setCreateTarget(false);
		mxCodec codec = new mxCodec();
		Document doc = mxUtils.loadDocument(Main.class.getResource("/daccosim-style.xml").toString());
		codec.decode(doc.getDocumentElement(), graph.getStylesheet());
		getViewport().setOpaque(true);
		getViewport().setBackground(Color.WHITE);
	}

	public Object[] importCells(Object[] cells, double dx, double dy, Object target, Point location) {
		mxICell cell = (mxICell) cells[0];
		if (target == null && cells.length == 1 && location != null) {
			target = getCellAt(location.x, location.y);

			if (target instanceof mxICell && cells[0] instanceof mxICell) {
				mxICell targetCell = (mxICell) target;
				mxICell dropCell = cell;

				if (targetCell.isVertex() == dropCell.isVertex() || targetCell.isEdge() == dropCell.isEdge()) {
					mxIGraphModel model = graph.getModel();
					model.setStyle(target, model.getStyle(cells[0]));
					graph.setSelectionCell(target);
					return null;
				}
			}
		}

		if (!(cell.getValue() instanceof String)) return super.importCells(cells, dx, dy, target, location);

		if (cell.getStyle().equals(EditorUtils.FMU)) {
			cell.setValue(Dialogs.showFmuForm(this, new FmuSpec()));
			if (cell.getValue() == null) return null;
			else return super.importCells(cells, dx, dy, target, location);
		}
		if (cell.getStyle().equals(EditorUtils.ADDER)) cell.setValue(new AdderSpec());
		if (cell.getStyle().equals(EditorUtils.OFFSET)) cell.setValue(new OffsetSpec());
		if (cell.getStyle().equals(EditorUtils.MULTIPLIER)) cell.setValue(new MultiplierSpec());
		if (cell.getStyle().equals(EditorUtils.GAIN)) cell.setValue(new GainSpec());
		if (cell.getStyle().equals(EditorUtils.EXTERNAL_INPUT)) cell.setValue(new ExternalInputSpec());
		if (cell.getStyle().equals(EditorUtils.EXTERNAL_OUTPUT)) cell.setValue(new ExternalOutputSpec());
		if (cell.isEdge()) cell.setValue(new ArrowSpec());

		return super.importCells(cells, dx, dy, target, location);
	}

	@Override
	protected mxSelectionCellsHandler createSelectionCellsHandler() {
		return new mxSelectionCellsHandler(this){

			@Override
			public void refresh() {
				super.refresh();
				EditorUtils.getEditor(GraphComponent.this).processStatus();
			}
		};
	}
}
