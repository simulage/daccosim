/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor.specs;

import eu.simulage.daccosim.DataType;

import java.io.Serializable;

public class VariableSpec implements Serializable {

	private String name;
	private String type;
	private String value;

	public VariableSpec() {
	}

	public VariableSpec(String name, String type, String value) {
		this.name = name;
		this.type = type;
		this.value = value;
	}

	public VariableSpec(String name, DataType type, String value) {
		this.name = name;
		this.type = type.toString();
		this.value = value;
	}

	public VariableSpec(VariableSpec toBeCloned) {
		this(toBeCloned.name, toBeCloned.type, toBeCloned.value);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public DataType dataType() {
		return DataType.valueOf(type);
	}
}
