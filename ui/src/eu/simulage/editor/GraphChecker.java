/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor;

import eu.simulage.daccosim.view.Operator;
import eu.simulage.editor.specs.*;

import java.util.HashSet;
import java.util.Set;

import static eu.simulage.editor.EditorUtils.*;

class GraphChecker {
	@SuppressWarnings("RedundantIfStatement")
	static boolean checkGraph(Editor editor) {
		if (editor == null) return false;
		if (!fmuPathsExist(editor)) return false;
		if (!thereAreVertexes(editor)) return false;
		if (idsAreRepeated(editor)) return false;
		if (!arrowsAreConnected(editor)) return false;
		if (!arrowsHaveConnections(editor)) return false;
		if (operatorInputNotConnected(editor)) return false;
		if (operatorOutputNotConnected(editor)) return false;
		if (externalOutputNotConnected(editor)) return false;
		if (externalInputNotConnected(editor)) return false;
		return true;
	}

	private static boolean fmuPathsExist(Editor editor) {
		Spec spec = vertexesOf().stream().filter(v -> isAnFmu(v) && (((FmuSpec) v).fileName().isEmpty() || !((FmuSpec) v).file().exists())).findFirst().orElse(null);
		if (spec == null) return true;
		showErrorDialog(editor, "Path of fmu " + spec.getLabel() + " is wrong");
		return false;
	}

	private static boolean thereAreVertexes(Editor editor) {
		if (vertexesOf().size() > 0) return true;
		showErrorDialog(editor, "Graph does not have any block");
		return false;
	}

	private static boolean idsAreRepeated(Editor editor) {
		Set<String> ids = new HashSet<>();
		for (Spec spec : vertexesOf()) {
			String label = spec.getLabel();
			if (!(spec instanceof Operator))
				if (!ids.contains(label)) ids.add(label);
				else {
					showErrorDialog(editor, "Some blocks have the same id: " + label);
					return true;
				}
		}
		return false;
	}

	private static boolean arrowsAreConnected(Editor editor) {
		boolean match = arrowsOf().stream().allMatch(ArrowSpec::isConnected);
		if (!match) showErrorDialog(editor, "Some arrows are not connected");
		return match;
	}

	private static boolean arrowsHaveConnections(Editor editor) {
		boolean match = arrowsOf().stream().noneMatch(a -> a.getConnections().isEmpty());
		if (!match) showErrorDialog(editor, "Some arrows have no connections defined");
		return match;
	}

	private static boolean operatorInputNotConnected(Editor editor) {
		Set<Spec> specSet = arrowTargets();
		boolean match = vertexesOf().stream().anyMatch(c -> isAnOperator(c) && !specSet.contains(c));
		if (match) showErrorDialog(editor, "Some operators have no input connected");
		return match;
	}

	private static boolean isAnOperator(Spec spec) {
		return spec instanceof OperatorSpec;
	}

	private static boolean isAnFmu(Spec spec) {
		return spec instanceof FmuSpec;
	}

	private static boolean operatorOutputNotConnected(Editor editor) {
		Set<Spec> specSet = EditorUtils.arrowSources();
		boolean match = vertexesOf().stream().anyMatch(c -> isAnOperator(c) && !specSet.contains(c));
		if (match) showErrorDialog(editor, "Some operators have no output connected");
		return match;
	}

	private static boolean externalInputNotConnected(Editor editor) {
		Set<Spec> specSet = EditorUtils.arrowSources();
		boolean match = vertexesOf().stream().anyMatch(c -> c instanceof ExternalInputSpec && !specSet.contains(c));
		if (match) showErrorDialog(editor, "Some external inputs are not connected");
		return match;
	}

	private static boolean externalOutputNotConnected(Editor editor) {
		Set<Spec> specSet = arrowTargets();
		boolean match = vertexesOf().stream().anyMatch(c -> c instanceof ExternalOutputSpec && !specSet.contains(c));
		if (match) showErrorDialog(editor, "Some external outputs are not connected");
		return match;
	}

}
