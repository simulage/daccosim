/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor;

import eu.simulage.MxGraphToDsg;
import eu.simulage.daccosim.view.FMU;
import eu.simulage.daccosim.view.Graph;
import org.javafmi.builder.Platform;
import org.javafmi.builder.ShellFmuBuilder;
import siani.javafmi.Matryoshka;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static eu.simulage.daccosim.GraphExecutor.logger;
import static eu.simulage.daccosim.Stepper.Method.ConstantStep;
import static eu.simulage.editor.EditorUtils.daccosimJarBytes;
import static eu.simulage.editor.EditorUtils.getEditor;

public class FmuExporter {
	public static void exportFmu(File selectedFile, List<Platform> platforms) {
		try {
			logger().info("Exporting fmu");
			Editor editor = EditorUtils.getEditor();
			File folder = Files.createTempDirectory("matryoshka-res").toFile();
			folder.deleteOnExit();
			Graph graph = MxGraphToDsg.toDaccosimGraph(editor);
			String[] filesToInclude = fmuFiles(graph);
			modify(graph);
			File modelFile = new File(folder, "res/model.dsg");
			modelFile.getParentFile().mkdirs();
			Files.write(modelFile.toPath(), graph.serialize());
			File jarFile = new File(folder, Matryoshka.class.getSimpleName() + ".jar");
			Files.write(jarFile.toPath(), daccosimJarBytes());
			jarFileUpdateWith(jarFile, modelFile);
			logger().info("Building fmu");
			File fmuFile = new ShellFmuBuilder(jarFile.getAbsolutePath())
					.fmuName(Matryoshka.class.getSimpleName())
					.platforms(platforms)
					.includes(filesToInclude)
					.build();
			modifyFmu(fmuFile, selectedFile);
			logger().info("Fmu exported: " + selectedFile.getAbsolutePath());
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	private static void modify(Graph graph) {
		graph.nodes().stream()
				.filter(n -> n instanceof FMU)
				.map(n -> (FMU) n)
				.forEach(n -> n.path(new File(n.path()).getName()));
		graph.settings().clearCoInitialization();
		graph.settings().stepper(ConstantStep);
		graph.settings().stopTime(Double.MAX_VALUE);
		graph.settings().startTime(0);
		graph.clearExport();
	}

	private static String[] fmuFiles(Graph graph) {
		return graph.nodes().stream()
				.filter(n -> n instanceof FMU)
				.map(n -> getEditor().getFile().fmuFile(new File(((FMU) n).path()).getName()).getAbsolutePath()).toArray(String[]::new);
	}

	public static void jarFileUpdateWith(File jarFile, File newFile) {
		Map<String, String> env = new HashMap<>();
		env.put("create", "true");
		try (FileSystem zipfs = FileSystems.newFileSystem(URI.create("jar:" + jarFile.toURI().toString()), env)) {
			Path pathInZipfile = zipfs.getPath("/" + newFile.getName());
			Files.copy(newFile.toPath(), pathInZipfile, StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static void modifyFmu(File fmuFile, File selectedFile) {
		Map<String, String> env = new HashMap<>();
		env.put("create", "true");
		try (FileSystem zipfs = FileSystems.newFileSystem(URI.create("jar:" + fmuFile.toURI().toString()), env)) {
			Path pathInZipfile = zipfs.getPath("/modelDescription.xml");
			String content = new String(Files.readAllBytes(pathInZipfile));
			content = content.replaceAll("modelName=\"[^\"]*\"", "modelName=\"" + filenameNoExtension(selectedFile) + "\"");
			content = content.replaceAll("generationTool=\"[^\"]*\"", "generationTool=\"Daccosim NG " + Editor.YEAR + "-" + Editor.VERSION + "\"");
			content = content.replaceAll("modelIdentifier=\"[^\"]*\"", "modelIdentifier=\"Matryoshka_" + filenameNoExtension(selectedFile) + "\"");
			Files.write(pathInZipfile, content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
			moveFile(zipfs, "/binaries/win64/Matryoshka.dll", "/binaries/win64/Matryoshka_" + filenameNoExtension(selectedFile) + ".dll");
			moveFile(zipfs, "/binaries/linux64/Matryoshka.so", "/binaries/linux64/Matryoshka_" + filenameNoExtension(selectedFile) + ".so");
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Files.move(fmuFile.toPath(), selectedFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void moveFile(FileSystem zipfs, String first, String second) {
		try {
			Files.move(zipfs.getPath(first), zipfs.getPath(second));
		} catch (IOException ignored) {
		}
	}

	private static String filenameNoExtension(File selectedFile) {
		return selectedFile.getName().replace(".fmu", "");
	}

}
