/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor.views;

import eu.simulage.daccosim.DataType;
import eu.simulage.editor.specs.OffsetSpec;

import javax.swing.*;
import java.awt.*;

import static eu.simulage.editor.DialogUtils.*;
import static eu.simulage.editor.EditorUtils.*;
import static java.awt.GridBagConstraints.*;
import static java.awt.GridBagConstraints.CENTER;
import static java.awt.GridBagConstraints.HORIZONTAL;

public class OffsetPropertiesDisplay extends JPanel {

	private JDialog dialog;
	private OffsetSpec spec;
	private JTextField label;
	private JComboBox<String> type;
	private JTextField value;

	public OffsetPropertiesDisplay(JDialog dialog, OffsetSpec spec) {
		this.dialog = dialog;
		this.spec = spec;
		setLayout(new GridBagLayout());
		add(label("Label: "),
				new GridBagConstraints(0, 0, 1, 1, 0, 0, LINE_START, NONE, new Insets(0, 10, 0, 0), 0, 0));
		add(label = new JTextField(this.spec.getLabel().isEmpty() ? "" : this.spec.getLabel()),
				new GridBagConstraints(1, 0, 1, 1, 1, 0, CENTER, HORIZONTAL, new Insets(0, 5, 0, 10), 0, 0));

		add(label("Type: "),
				new GridBagConstraints(0, 1, 1, 1, 0, 0, LINE_START, NONE, new Insets(5, 10, 0, 0), 0, 0));
		add(type = operableTypesComboBox(),
				new GridBagConstraints(1, 1, 1, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 10), 0, 0));
		type.setSelectedItem(this.spec.getType());

		add(label("Offset value: "),
				new GridBagConstraints(0, 2, 1, 1, 0, 0, LINE_START, NONE, new Insets(5, 10, 0, 0), 0, 0));
		add(value = new JTextField(this.spec.getLabel()),
				new GridBagConstraints(1, 2, 1, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 10), 0, 0));
		value.setText(this.spec.getValue());
		setVisible(true);
	}

	private JPanel offsetLabel() {
		JPanel panel = new JPanel(new FlowLayout());
		panel.add(label("Label: "));
		panel.add(label = new JTextField(spec.getLabel().isEmpty() ? "" : spec.getLabel()));
		label.setPreferredSize(new Dimension(400, 20));
		label.setMaximumSize(new Dimension(400, 20));
		return panel;
	}

	private JPanel comboBox() {
		JPanel panel = new JPanel();
		panel.add(label("Type: "));
		panel.add(type = operableTypesComboBox());
		type.setSelectedItem(spec.getType());
		type.setPreferredSize(new Dimension(400, 20));
		return panel;
	}

	@SuppressWarnings("Duplicates")
	private JPanel value() {
		JPanel panel = new JPanel(new FlowLayout());
		panel.add(label("Offset value: "));
		panel.add(value = new JTextField(spec.getLabel()));
		value.setText(spec.getValue());
		value.setPreferredSize(new Dimension(400, 20));
		return panel;
	}

	public void saveForm() {
		if (!checkers.get(DataType.valueOf(type.getSelectedItem().toString())).check(value.getText())) {
			showErrorDialog(dialog, "Offset value (" + value.getText() + ") not valid for its type (" + type.getSelectedItem().toString() + ")");
			return;
		}
		if (!label.getText().isEmpty()) spec.setLabel(label.getText());
		spec.setType(type.getSelectedItem().toString());
		spec.setValue(value.getText());
		spec.getInputs().get(0).setType(spec.getType());
		spec.getInputs().get(0).setValue(defaultValueOf(spec.getType()));
		removeConnectionsAndOutputs(spec);
		getEditor().setModified(true);
		dialog.dispose();
	}

	private JLabel label(String text) {
		JLabel jLabel = new JLabel(text);
		jLabel.setFont(new Font("arial", Font.BOLD, 12));
		jLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		jLabel.setPreferredSize(new Dimension(80, 20));
		return jLabel;
	}

}

