/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor.views;

import eu.simulage.daccosim.DataType;
import eu.simulage.editor.DialogUtils;
import eu.simulage.editor.EditorUtils;
import eu.simulage.editor.Spec;
import eu.simulage.editor.specs.VariableSpec;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static eu.simulage.editor.DialogUtils.addTooltip;
import static eu.simulage.editor.DialogUtils.checkers;
import static eu.simulage.editor.EditorUtils.showErrorDialog;

abstract class ExternalPropertiesDisplay extends JPanel {
	protected JTextField label;
	JTable variables;
	JDialog dialog;

	ExternalPropertiesDisplay(JDialog dialog) {
		this.dialog = dialog;
	}

	static String typeAt(JTable table, int row) {
		return table.getValueAt(row, 0).toString();
	}

	static String nameAt(JTable table, int row) {
		return table.getValueAt(row, 1).toString();
	}

	static String valueAt(JTable table, int row) {
		return table.getValueAt(row, 2).toString();
	}

	Component connectors(List<String> headers, List<VariableSpec> connectorList) {
		variables = new JTable(new DefaultTableModel(headers.toArray(new String[headers.size()]), 0));
		addTooltip(variables);
		variables.getColumn("Type").setCellEditor(new DefaultCellEditor(DialogUtils.typeComboBox()));
		variables.getTableHeader().setBackground(Color.LIGHT_GRAY);
		variables.getTableHeader().setOpaque(false);
		variables.getColumnModel().getColumn(0).setMaxWidth(50);
		variables.getColumnModel().getColumn(2).setMaxWidth(70);
		connectorList.forEach(c -> ((DefaultTableModel) variables.getModel()).addRow(new Object[]{c.getType(), c.getName(), c.getValue()}));
		JScrollPane jScrollPane = new JScrollPane(variables);
		jScrollPane.setPreferredSize(new Dimension(440, 450));
		return jScrollPane;
	}

	void addRow() {
		((DefaultTableModel) variables.getModel()).addRow(new String[]{DataType.Real.toString(), newVariableName(), "0.0"});
	}

	private String newVariableName() {
		return String.format("%s%03d", varNamePrefix(), (variables.getRowCount() + 1));
	}

	abstract String varNamePrefix();

	void removeRow() {
		while (variables.getSelectedRow() != -1)
			((DefaultTableModel) variables.getModel()).removeRow(variables.getSelectedRow());
	}

	boolean checkForm(Spec spec) {
		return labelIsCorrect(spec) && outputsAreCorrect();
	}

	private boolean labelIsCorrect(Spec spec) {
		if (label.getText().isEmpty()) {
			showErrorDialog(dialog, "label cannot be empty");
			return false;
		}
		if (EditorUtils.labelCount(label.getText(), spec) > 0) {
			showErrorDialog(dialog, "this label already exists");
			return false;
		}
		return true;
	}

	private boolean outputsAreCorrect() {
		return checkOutputNames() && checkOutputValues();
	}

	private boolean checkOutputNames() {
		Set<String> names = new HashSet<>();
		for (int i = 0; i < variables.getRowCount(); i++) {
			if (names.contains(nameAt(variables, i))) {
				showErrorDialog(dialog, "output name " + nameAt(variables, i) + " is repeated");
				return false;
			} else names.add(nameAt(variables, i));
		}
		return true;
	}

	private boolean checkOutputValues() {
		for (int i = 0; i < variables.getRowCount(); i++) {
			if (checkers.get(DataType.valueOf(typeAt(variables, i))).check(valueAt(variables, i))) continue;
			showErrorDialog(dialog, nameAt(variables, i) + " has a value (" + valueAt(variables, i) + ") not valid for its type (" + typeAt(variables, i) + ")");
			return false;
		}
		return true;
	}

}
