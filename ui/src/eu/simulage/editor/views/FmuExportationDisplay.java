/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor.views;

import eu.simulage.editor.DaccosimDialog;
import eu.simulage.editor.EditorUtils;
import eu.simulage.editor.FmuExporter;
import org.javafmi.builder.Platform;
import org.javafmi.kernel.OS;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FmuExportationDisplay extends JPanel {

	private final DaccosimDialog dialog;
	private File file;
	private JCheckBox win64Check;
	private JCheckBox linux64Check;
	private JLabel link;

	public FmuExportationDisplay(DaccosimDialog dialog, File file) {
		this.dialog = dialog;
		this.file = file;
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(checkers());
		add(finishedLabel());
	}

	private Component checkers() {
		JPanel panel = new JPanel();
		panel.add(win64Check());
		panel.add(Box.createRigidArea(new Dimension(50, 0)));
		panel.add(linux64Check());
		return panel;
	}

	public void okButton() {
		if (!win64Check.isSelected() && !linux64Check.isSelected()) {
			EditorUtils.showErrorDialog(dialog, "At least one platform must be selected for exportation");
			return;
		}
		EditorUtils.doInBackground(dialog, () -> {
			FmuExporter.exportFmu(file, platforms());
			link.setVisible(true);
			dialog.okButton().setVisible(false);
			dialog.cancelButton().setText("Close");
		});
	}

	private List<Platform> platforms() {
		List<Platform> platformList = new ArrayList<>();
		if (win64Check.isSelected()) platformList.add(Platform.Win64);
		if (linux64Check.isSelected()) platformList.add(Platform.Linux64);
		return platformList;
	}

	public void cancel() {
		dialog.dispose();
	}

	private JCheckBox win64Check() {
		return win64Check = new JCheckBox("Windows 64 bits", OS.isWin64());
	}

	private JCheckBox linux64Check() {
		return linux64Check = new JCheckBox("Linux 64 bits", OS.isLinux64());
	}

	private Component finishedLabel() {
		link = new JLabel("FMU exported: " + file.getName());
		link.setCursor(new Cursor(Cursor.HAND_CURSOR));
		link.setForeground(Color.BLUE);
		link.setAlignmentX(Component.CENTER_ALIGNMENT);
		link.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Desktop.getDesktop().open(file.getParentFile());
					dialog.dispose();
				} catch (IOException ignored) {
				}
			}
		});
		link.setVisible(false);
		return link;
	}
}

