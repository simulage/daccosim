/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor.views;

import eu.simulage.editor.EditorUtils;
import eu.simulage.editor.specs.ConfigurationSpec;
import eu.simulage.editor.specs.FmuSpec;
import org.javafmi.wrapper.Simulation;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

import static eu.simulage.SimulationProvider.getSimulation;
import static eu.simulage.editor.DialogUtils.*;
import static eu.simulage.editor.EditorUtils.*;
import static java.awt.GridBagConstraints.*;
import static java.awt.event.ItemEvent.SELECTED;
import static java.lang.Double.parseDouble;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toMap;
import static java.util.stream.IntStream.range;

public class ConfigurationDisplay extends JPanel {

	private static final String[] DECIMAL_SEPARATORS_OPT_1 = new String[]{".", ","};
	private static final String[] DECIMAL_SEPARATORS_OPT_2 = new String[]{"."};
	private final JDialog dialog;
	private final JTabbedPane tabbedPane;
	private ConfigurationSpec spec;
	private JTextField startTime;
	private JTextField stopTime;
	private JTextField tolerance;
	private JTextField maxIter;
	private JComboBox<String> methodCombo;
	private JTextField stepSize;
	private JTextField minSize;
	private JTextField safetyFactor;
	private JComboBox order;
	private JTextField maxNumberOfSteps;
	private JComboBox<String> cellSeparator;
	private JComboBox<String> decimalSeparator;
	private JTable varToExport;
	private JTable stepperVars;

	public ConfigurationDisplay(JDialog dialog, ConfigurationSpec configurationSpec) {
		this.dialog = dialog;
		this.spec = configurationSpec;
		setLayout(new GridBagLayout());
		tabbedPane = new JTabbedPane();
		tabbedPane.add("General", generalPanel());
		tabbedPane.add("Vars to export", varsToExport());
		tabbedPane.add("Stepper vars", stepperVars());
		processMethodSelection(new ItemEvent(methodCombo, 1, spec.stepperMethod(), SELECTED));
		add(tabbedPane,
				new GridBagConstraints(0, 0, 1, 1, 1, 1, CENTER, BOTH, new Insets(5, 5, 5, 5), 0, 0));
		dialog.pack();
		setVisible(true);
	}

	private JPanel generalPanel() {
		JPanel panel = new JPanel(new GridBagLayout());

		panel.add(label("Start time: ", Font.PLAIN),
				new GridBagConstraints(0, 0, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(startTime = new JTextField(spec.startTime()),
				new GridBagConstraints(1, 0, 1, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		panel.add(label("Stop time: ", Font.PLAIN),
				new GridBagConstraints(2, 0, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 10, 0, 0), 0, 0));
		panel.add(stopTime = new JTextField(spec.stopTime()),
				new GridBagConstraints(3, 0, 1, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));

		panel.add(label("Co-Init", Font.BOLD, 14),
				new GridBagConstraints(0, 1, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(20, 0, 0, 0), 0, 0));
		panel.add(new JSeparator(SwingConstants.HORIZONTAL),
				new GridBagConstraints(1, 1, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(20, 5, 0, 0), 0, 0));

		panel.add(label("Tolerance: ", Font.PLAIN),
				new GridBagConstraints(0, 2, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(tolerance = new JTextField(spec.tolerance()),
				new GridBagConstraints(1, 2, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		tolerance.setEnabled(spec.coInit());

		panel.add(label("Max Iterations: ", Font.PLAIN),
				new GridBagConstraints(0, 3, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(maxIter = new JTextField(spec.maxIter()),
				new GridBagConstraints(1, 3, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		maxIter.setEnabled(spec.coInit());

		panel.add(label("Stepper", Font.BOLD, 14),
				new GridBagConstraints(0, 4, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(20, 0, 0, 0), 0, 0));
		panel.add(new JSeparator(SwingConstants.HORIZONTAL),
				new GridBagConstraints(1, 4, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(20, 5, 0, 0), 0, 0));

		panel.add(label("Method: ", Font.PLAIN),
				new GridBagConstraints(0, 5, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(methodCombo = new JComboBox<>(new String[]{CONSTANT, EULER, ADAMS}),
				new GridBagConstraints(1, 5, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		methodCombo.setSelectedItem(spec.stepperMethod());
		methodCombo.addItemListener(this::processMethodSelection);

		panel.add(label("Step size: ", Font.PLAIN),
				new GridBagConstraints(0, 6, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(stepSize = new JTextField(spec.stepSize()),
				new GridBagConstraints(1, 6, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));

		panel.add(label("Min step: ", Font.PLAIN),
				new GridBagConstraints(0, 7, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(minSize = new JTextField(spec.minStep()),
				new GridBagConstraints(1, 7, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));

		panel.add(label("Safety factor: ", Font.PLAIN),
				new GridBagConstraints(0, 8, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(safetyFactor = new JTextField(spec.safetyFactor()),
				new GridBagConstraints(1, 8, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));

		panel.add(label("Order: ", Font.PLAIN),
				new GridBagConstraints(0, 9, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(order = new JComboBox<>(new String[]{"3", "4", "5", "6", "7", "8", "9"}),
				new GridBagConstraints(1, 9, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		order.setSelectedItem(spec.order());

		panel.add(label("Max No. steps: ", Font.PLAIN),
				new GridBagConstraints(0, 10, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(maxNumberOfSteps = new JTextField(spec.maxNumberOfSteps()),
				new GridBagConstraints(1, 10, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));

		panel.add(label("Export", Font.BOLD, 14),
				new GridBagConstraints(0, 11, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(20, 0, 0, 0), 0, 0));
		panel.add(new JSeparator(SwingConstants.HORIZONTAL),
				new GridBagConstraints(1, 11, 3, 1, 1, 0, CENTER, HORIZONTAL, new Insets(20, 5, 0, 0), 0, 0));

		panel.add(label("Cell sep: ", Font.PLAIN),
				new GridBagConstraints(0, 12, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 0, 0, 0), 0, 0));
		panel.add(this.cellSeparator = cellSeparatorCombo(),
				new GridBagConstraints(1, 12, 1, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		panel.add(label("Decimal sep: ", Font.PLAIN),
				new GridBagConstraints(2, 12, 1, 1, 0, 0, LINE_START, HORIZONTAL, new Insets(5, 10, 0, 0), 0, 0));
		panel.add(this.decimalSeparator = new JComboBox<>(DECIMAL_SEPARATORS_OPT_1),
				new GridBagConstraints(3, 12, 1, 1, 1, 0, CENTER, HORIZONTAL, new Insets(5, 5, 0, 0), 0, 0));
		this.cellSeparator.setSelectedItem(spec.cellSeparator());
		this.decimalSeparator.setSelectedItem(spec.decimalSeparator());
		return panel;
	}

	private JComboBox<String> cellSeparatorCombo() {
		JComboBox<String> cellSeparator = new JComboBox<>(new String[]{";", ","});
		cellSeparator.addItemListener(e -> {
			if (e.getStateChange() == SELECTED && e.getItem().toString().equals(",")) {
				decimalSeparator.setModel(new DefaultComboBoxModel<>(DECIMAL_SEPARATORS_OPT_2));
			}
			if (e.getStateChange() == SELECTED && e.getItem().toString().equals(";")) {
				decimalSeparator.setModel(new DefaultComboBoxModel<>(DECIMAL_SEPARATORS_OPT_1));
			}
		});
		return cellSeparator;
	}

	private JPanel varsToExport() {
		JPanel panel = new JPanel(new GridBagLayout());
		panel.add(new JScrollPane(varToExport = createVariablesToExportMap(spec.variablesToExport())),
				new GridBagConstraints(0, 0, 1, 1, 1, 1, CENTER, BOTH, new Insets(0, 5, 0, 5), 0, 0));
		panel.add(removeAddButtons((e) -> addVarToExport(), (e) -> removeVarToExport()),
				new GridBagConstraints(0, 1, 1, 1, 1, 0, LINE_START, NONE, new Insets(0, 0, 0, 0), 0, 0));
		return panel;
	}

	private JPanel stepperVars() {
		JPanel panel = new JPanel(new GridBagLayout());
		panel.add(new JScrollPane(stepperVars = createVariablesStepperMap(spec.stepperVariables())),
				new GridBagConstraints(0, 0, 1, 1, 1, 1, CENTER, BOTH, new Insets(0, 5, 0, 5), 0, 0));
		panel.add(removeAddButtons((e) -> addStepperVar(), (e) -> removeStepperVar()),
				new GridBagConstraints(0, 1, 1, 1, 1, 0, LINE_START, NONE, new Insets(0, 0, 0, 0), 0, 0));
		return panel;
	}

	private JTable createVariablesToExportMap(List<String> variablesToExport) {
		DefaultTableModel model = new DefaultTableModel(new String[]{"Variable name"}, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		JTable table = new JTable(model);
		addTooltip(table);
		addSorter(table);
		table.getTableHeader().setBackground(Color.LIGHT_GRAY);
		table.getTableHeader().setOpaque(false);
		variablesToExport.forEach(v -> model.addRow(new String[]{v}));
		return table;
	}

	private JTable createVariablesStepperMap(Map<String, Double> content) {
		DefaultTableModel model = new DefaultTableModel(new String[]{"Variable name", "Tolerance"}, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		JTable table = new JTable(model);
		addTooltip(table);
		addSorter(table);
		table.getColumn("Tolerance").setMaxWidth(70);
		table.getTableHeader().setBackground(Color.LIGHT_GRAY);
		table.getTableHeader().setOpaque(false);
		content.forEach((k, v) -> model.addRow(new String[]{k, v + ""}));
		return table;
	}

	private void addVarToExport() {
		Set<String> alreadySelected = new HashSet<>(range(0, varToExport.getRowCount()).boxed()
				.map(i -> varToExport.getValueAt(i, 0).toString()).collect(Collectors.toList()));
		List<String> exportableVariables = allVariables().stream()
				.filter(v -> !alreadySelected.contains(v))
				.collect(Collectors.toList());
		SelectorDialog dialog = new SelectorDialog(this.dialog, exportableVariables, true);
		dialog.selectedItems()
				.forEach(c -> ((DefaultTableModel) varToExport.getModel()).addRow(new String[]{c}));
	}

	private void removeVarToExport() {
		while (varToExport.getSelectedRow() != -1)
			((DefaultTableModel) varToExport.getModel()).removeRow(varToExport.convertRowIndexToModel(varToExport.getSelectedRow()));
	}


	private void addStepperVar() {
		Set<String> alreadySelected = new HashSet<>(range(0, stepperVars.getRowCount()).boxed()
				.map(i -> stepperVars.getValueAt(i, 0).toString()).collect(Collectors.toList()));
		List<String> variablesForStepper = stepperVariables().stream()
				.filter(v -> !alreadySelected.contains(v))
				.collect(Collectors.toList());
		SelectorDialog dialog = new SelectorDialog(this.dialog, variablesForStepper, true);
		dialog.selectedItems()
				.forEach(c -> ((DefaultTableModel) stepperVars.getModel()).addRow(new String[]{c, "1E-5"}));
	}

	private void removeStepperVar() {
		while (stepperVars.getSelectedRow() != -1)
			((DefaultTableModel) stepperVars.getModel()).removeRow(stepperVars.convertRowIndexToModel(stepperVars.getSelectedRow()));
	}

	private List<String> allVariables() {
		List<String> result = new ArrayList<>();
		getCellSpecs().forEach(c -> {
			result.addAll(c.qnInputs());
			result.addAll(c.qnOutputs());
			result.addAll(c.qnVariables());
		});
		return result;
	}

	private List<String> stepperVariables() {
		List<String> result = new ArrayList<>();
		getCellSpecs().filter(s -> s instanceof FmuSpec).map(s -> (FmuSpec) s).forEach(s -> {
			Simulation simulation = getSimulation(s.file());
			if (simulation == null) return;
			Set<String> variables = new HashSet<>();
			stream(simulation.getModelDescription().getModelVariables()).forEach(v -> variables.add(v.getName()));
			stream(simulation.getModelDescription().getModelVariables())
					.filter(v -> variables.contains("der(" + v.getName() + ")"))
					.forEach(v -> result.add(s.label() + "." + v.getName()));
		});
		return result;
	}

	private void processMethodSelection(ItemEvent e) {
		if (e.getStateChange() == SELECTED && e.getItem().toString().equals(CONSTANT)) {
			tabbedPane.setEnabledAt(2, false);
			stream(new Component[]{stepSize}).forEach(c -> c.setEnabled(true));
			stream(new Component[]{minSize, safetyFactor, order, maxNumberOfSteps})
					.forEach(c -> c.setEnabled(false));
		}
		if (e.getStateChange() == SELECTED && e.getItem().toString().equals(EULER)) {
			prepareStepperVariablesTab();
			stream(new Component[]{minSize, safetyFactor, maxNumberOfSteps})
					.forEach(c -> c.setEnabled(true));
			stream(new Component[]{stepSize, order}).forEach(c -> c.setEnabled(false));
		}
		if (e.getStateChange() == SELECTED && e.getItem().toString().equals(ADAMS)) {
			prepareStepperVariablesTab();
			stream(new Component[]{minSize, safetyFactor, maxNumberOfSteps, order})
					.forEach(c -> c.setEnabled(true));
			stream(new Component[]{stepSize}).forEach(c -> c.setEnabled(false));
		}
	}

	private void prepareStepperVariablesTab() {
		tabbedPane.setEnabledAt(2, true);
		while (stepperVars.getRowCount() > 0) ((DefaultTableModel) stepperVars.getModel()).removeRow(0);
		stepperVariables().forEach(v -> ((DefaultTableModel) stepperVars.getModel()).addRow(new String[]{v, "1E-5"}));
	}

	@SuppressWarnings("ConstantConditions")
	public void saveForm() {
		if (!checkIsDouble("startTime", startTime)) return;
		if (!checkIsDouble("stopTime", stopTime)) return;
		if (!checkStopAndStartTimeAreCorrect()) return;
		if (!checkIsDouble("tolerance", tolerance)) return;
		if (!checkIsInt("maxIter", maxIter)) return;
		if (!checkCorrectStepSize()) return;
		if (!checkMinStep()) return;
		if (!checkSafetyFactor()) return;
		if (!checkVariableStepperList()) return;
		if (!checkIsInt("maxNumberOfSteps", maxNumberOfSteps)) return;
		spec.startTime(startTime.getText());
		spec.stopTime(stopTime.getText());
		spec.tolerance(tolerance.getText());
		spec.maxIter(maxIter.getText());
		spec.stepperMethod(methodComboItem());
		spec.stepSize(stepSize.getText());
		spec.minStep(minSize.getText());
		spec.safetyFactor(safetyFactor.getText());
		spec.order(order.getSelectedItem().toString());
		spec.maxNumberOfSteps(maxNumberOfSteps.getText());
		spec.cellSeparator(cellSeparator.getSelectedItem().toString());
		spec.decimalSeparator(decimalSeparator.getSelectedItem().toString());
		spec.variablesToExport(range(0, varToExport.getRowCount()).boxed()
				.map(i -> varToExport.getValueAt(i, 0).toString()).collect(Collectors.toList()));
		spec.stepperVariables(spec.stepperMethod().equals(CONSTANT) ? emptyMap() : range(0, stepperVars.getRowCount()).boxed()
				.collect(toMap(i -> stepperVars.getValueAt(i, 0).toString(), i -> Double.parseDouble(stepperVars.getValueAt(i, 1).toString()))));
		getEditor().setModified(true);
		dialog.dispose();
	}

	private boolean checkCorrectStepSize() {
		if (!checkIsDouble("stepSize", stepSize)) return false;
		if (parseDouble(stepSize.getText()) < 1e-200) {
			showErrorDialog(dialog, "Step size must be greater than 0");
			return false;
		}
		return true;
	}

	private boolean checkSafetyFactor() {
		if (!safetyFactor.isEnabled()) return true;
		if (!checkIsDouble("safetyFactor", safetyFactor)) return false;
		double safetyFactor = parseDouble(this.safetyFactor.getText());
		if (safetyFactor < 0.8 || safetyFactor > 0.9) {
			showErrorDialog(dialog, "Safety factor must be between 0.8 and 0.9");
			return false;
		}
		return true;
	}

	private boolean checkVariableStepperList() {
		if (CONSTANT.equals(methodComboItem())) return true;
		if (stepperVars.getRowCount() == 0) {
			showErrorDialog(dialog, "Variables for the stepper is empty. It must be filled when " + EULER + " or " + ADAMS + " is selected.");
			return false;
		}
		return true;
	}

	private boolean checkMinStep() {
		return !minSize.isEnabled() || checkIsDouble("minSize", minSize);
	}

	private String methodComboItem() {
		return methodCombo.getSelectedItem() != null ? methodCombo.getSelectedItem().toString() : null;
	}

	private boolean checkIsDouble(String fieldName, JTextField field) {
		if (!field.isEnabled() || isDouble(field.getText())) return true;
		showErrorDialog(dialog, "The field " + fieldName + " must be a real number");
		return false;
	}

	private boolean checkIsInt(String fieldName, JTextField field) {
		if (!field.isEnabled() || isInteger(field.getText())) return true;
		showErrorDialog(dialog, "The field " + fieldName + " must be an integer number");
		return false;
	}

	private boolean checkStopAndStartTimeAreCorrect() {
		double startTime = parseDouble(this.startTime.getText());
		double stopTime = parseDouble(this.stopTime.getText());
		if (stopTime < startTime) {
			showErrorDialog(dialog, "Stop time cannot be smaller than the start time");
			return false;
		}
		return true;
	}


	private JLabel label(String text, int style) {
		return label(text, style, 12);
	}

	private JLabel label(String text, int style, int size) {
		JLabel jLabel = new JLabel(text);
		jLabel.setFont(fontWith(style, size));
		jLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		return jLabel;
	}

	private Font fontWith(int style, int size) {
		return new Font("arial", style, size);
	}

	public void reset() {
		EditorUtils.setConfiguration(spec = new ConfigurationSpec());
		startTime.setText(spec.startTime());
		stopTime.setText(spec.stopTime());
		tolerance.setText(spec.tolerance());
		maxIter.setText(spec.maxIter());
		methodCombo.setSelectedItem(spec.stepperMethod());
		stepSize.setText(spec.stepSize());
		minSize.setText(spec.minStep());
		safetyFactor.setText(spec.safetyFactor());
		order.setSelectedItem(spec.order());
		maxNumberOfSteps.setText(spec.maxNumberOfSteps());
		cellSeparator.setSelectedItem(spec.cellSeparator());
		decimalSeparator.setSelectedItem(spec.decimalSeparator());
	}
}

