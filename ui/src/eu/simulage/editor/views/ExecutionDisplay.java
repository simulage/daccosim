/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor.views;

import eu.simulage.editor.DaccosimDialog;
import eu.simulage.editor.ExecutionThread;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class ExecutionDisplay extends JPanel {

	private final JDialog dialog;
	private final ExecutionThread thread;
	private JTextArea console;
	private JLabel link;

	public ExecutionDisplay(DaccosimDialog dialog, ExecutionThread thread) {
		this.dialog = dialog;
		this.thread = thread;
		dialog.okButton().setVisible(false);
		controlCloseButton();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(console());
		add(finishedLabel());
		thread.addLoggerListener(s -> console.append("\n" + s));
		thread.setOnFinishListener(() -> {
			if (thread.exportFile() != null) link.setText(wordWrap(thread.exportFile().getAbsolutePath()));
			link.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseClicked(MouseEvent e) {
					try {
						Desktop.getDesktop().open(thread.exportFile());
						dialog.dispose();
					} catch (IOException ignored) {
					}
				}
			});
			dialog.cancelButton().setText("Close");
			link.setVisible(true);
		});
		thread.execute();
	}

	private String wordWrap(String absolutePath) {
		StringBuilder builder = new StringBuilder("<html><center>");
		int lineSize = 80;
		while (absolutePath.length() > lineSize) {
			builder.append(absolutePath.substring(0, lineSize)).append("<br>");
			absolutePath = absolutePath.substring(lineSize);
		}
		return builder.append(absolutePath).append("</center></html>").toString();
	}

	private void controlCloseButton() {
		dialog.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				cancel();
			}
		});
	}

	public void okButton() {
		if (thread.isDone()) dialog.dispose();
	}

	public void cancel() {
		if (!thread.isDone()) thread.stop();
		dialog.dispose();
	}

	private JScrollPane console() {
		console = new JTextArea();
		console.setFont(new Font("arial", Font.PLAIN, 10));
		console.setEditable(false);
		JScrollPane pane = new JScrollPane(console);
		pane.setMinimumSize(new Dimension(480, 370));
		pane.setMaximumSize(new Dimension(480, 400));
		return pane;
	}

	private Component finishedLabel() {
		link = new JLabel("Simulation finished with no output. Select variables if wanted", SwingConstants.CENTER);
		link.setCursor(new Cursor(Cursor.HAND_CURSOR));
		link.setForeground(Color.BLUE);
		link.setAlignmentX(Component.CENTER_ALIGNMENT);
		link.setVisible(false);
		return link;
	}
}

