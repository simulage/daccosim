/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.editor.views;

import eu.simulage.daccosim.DataType;
import eu.simulage.editor.specs.ArrowSpec;
import eu.simulage.editor.specs.VariableSpec;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.simulage.editor.DialogUtils.*;
import static eu.simulage.editor.EditorUtils.getEditor;
import static eu.simulage.editor.EditorUtils.showErrorDialog;
import static java.awt.GridBagConstraints.*;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.IntStream.range;

public class ArrowPropertiesDisplay extends JPanel {

	private static final int INPUT_COLUMN = 2;
	private static final int OUTPUT_COLUMN = 1;
	private final JDialog dialog;
	private ArrowSpec spec;
	private JTable connections;

	public ArrowPropertiesDisplay(JDialog dialog, ArrowSpec spec) {
		this.dialog = dialog;
		this.spec = spec;
		setLayout(new GridBagLayout());
		add(connections(),
				new GridBagConstraints(0, 0, 1, 1, 1, 1, CENTER, BOTH, new Insets(0, 10, 0, 10), 0, 0));
		add(removeAddButtons((e) -> addConn(), (e) -> removeConn()),
				new GridBagConstraints(0, 1, 1, 1, 0, 0, LINE_START, NONE, new Insets(0, 5, 0, 0), 0, 0));
	}

	private void addConn() {
		if (connections.getSelectedRow() == -1) {
			showErrorDialog(this.dialog, "Select an input first");
			return;
		}
		Set<DataType> dataTypes = stream(connections.getSelectedRows()).boxed()
				.map(i -> targetVarSpec(connections.getValueAt(i, INPUT_COLUMN).toString()).dataType())
				.collect(toSet());
		if (dataTypes.size() > 1) {
			showErrorDialog(this.dialog, "Selected inputs must have the same data type");
			return;
		}
		showSelectorDialog(dataTypes);
	}

	private void showSelectorDialog(Set<DataType> dataTypes) {
		List<VariableSpec> specs = spec.allOutputs().stream()
				.filter(v -> v.dataType().equals(dataTypes.iterator().next()))
				.collect(Collectors.toList());
		SelectorDialog dialog = new SelectorDialog(this.dialog, specs.stream().map(VariableSpec::getName).collect(Collectors.toList()), false);
		if (dialog.selectedItems().isEmpty()) return;
		setSourceValue(dialog.selectedItems().get(0));
	}

	private void removeConn() {
		setSourceValue("");
	}

	private void setSourceValue(String value) {
		for (int index : connections.getSelectedRows()) connections.setValueAt(value, index, OUTPUT_COLUMN);
	}

	private Component connections() {
		connections = new JTable(new DefaultTableModel(new Object[]{"Type", spec.source().label() + " outputs", spec.target().label() + " inputs"}, 0) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		});
		addTooltip(connections);
		addSorter(connections);
		connections.getTableHeader().setBackground(Color.LIGHT_GRAY);
		connections.getTableHeader().setOpaque(false);
		connections.getColumnModel().getColumn(0).setMaxWidth(50);
		connections.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				if (e.getClickCount() == 2) addConn();
			}
		});
		spec.availableInputs()
				.forEach(v -> tableModel().addRow(new Object[]{v.dataType().toString(), "", v.getName()}));
		spec.getConnections()
				.forEach(c -> tableModel().addRow(new Object[]{targetVarSpec(c.getTo()).dataType().name(), c.getFrom(), c.getTo()}));
		return new JScrollPane(connections);
	}

	private DefaultTableModel tableModel() {
		return (DefaultTableModel) connections.getModel();
	}

	public void saveForm() {
		spec.getConnections().clear();
		range(0, connections.getRowCount())
				.filter(i -> !connections.getValueAt(i, OUTPUT_COLUMN).toString().isEmpty())
				.forEach(i -> spec.addConnection(connections.getValueAt(i, OUTPUT_COLUMN).toString(),
						connections.getValueAt(i, INPUT_COLUMN).toString()));
		getEditor().setModified(true);
		dialog.dispose();
	}

	private VariableSpec targetVarSpec(String value) {
		return spec.allInputs().stream().filter(v -> v.getName().equals(value)).findFirst().get();
	}

	private VariableSpec sourceVarSpec(String value) {
		return spec.allOutputs().stream().filter(v -> v.getName().equals(value)).findFirst().get();
	}

}

