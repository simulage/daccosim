package eu.simulage.shell;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.Scanner;

import static java.nio.file.Files.readAllBytes;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.time.format.DateTimeFormatter.ofPattern;

class PackageAll {
	private static String wd = "";
	private static String version = "vX.X.X-Test";
	private static String year = "2018";
	private static String editorContent;

	public static void main(String[] args) {
		readArgs(args);
		changeVersionAndDateAtEditor();
		runMaven();
		resetEditorFile();
		createExeFiles();
		renameJarFiles();
		deployToBitBucket();
		System.out.println("Process finished");
	}

	private static void readArgs(String[] args) {
		wd = args.length > 0 ? args[0] : wd;
		wd = wd.endsWith("\\") || wd.endsWith("/") ? wd : wd + "";
		version = args.length > 1 ? args[1] : version;
		year = args.length > 2 ? args[2] : year;
	}

	private static void resetEditorFile() {
		try {
			Files.write(editorFile().toPath(), editorContent.getBytes(), TRUNCATE_EXISTING);
			System.out.println("Editor written to contain marks");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void changeVersionAndDateAtEditor() {
		try {
			editorContent = new String(readAllBytes(editorFile().toPath()));
			String content = editorContent.replace("%%VERSION%%", version);
			content = content.replace("%%DATE%%", LocalDate.now().format(ofPattern("dd-MM-yyyy")));
			content = content.replace("%%YEAR%%", year);
			Files.write(editorFile().toPath(), content.getBytes(), TRUNCATE_EXISTING);
			System.out.println("Edited version and date of the editor");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private static File editorFile() {
		return new File(wd + "ui/src/eu/simulage/editor/Editor.java");
	}

	private static void runMaven() {
		try {
			execProcess("mvn.bat -f \"" + wd + "core\" install");
			execProcess("mvn.bat -f \"" + wd + "shell\" install");
			Files.copy(new File(wd + "shell/target/shell-jar-with-dependencies.jar").toPath(),
					new File(wd + "ui/res/Matryoshka.jar").toPath(), StandardCopyOption.REPLACE_EXISTING);
			execProcess("mvn.bat -f \"" + wd + "ui\" package");
			System.out.println("All maven scripts executed");
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	}

	private static void execProcess(String command) throws InterruptedException, IOException {
		Process process = Runtime.getRuntime().exec(command);
		StreamGobbler gobbler = new StreamGobbler(process.getInputStream());
		gobbler.start();
		process.waitFor();
	}

	private static void createExeFiles() {
		try {
			execProcess("launch4jc \"" + wd + "ui/launch4jExeConfiguration32.xml\"");
			execProcess("launch4jc \"" + wd + "ui/launch4jExeConfiguration64.xml\"");
			System.out.println("Exe files created");
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}

	}

	private static void renameJarFiles() {
		move("shell/target/shell-jar-with-dependencies.jar", "shell/target/daccosim-shell-" + year + "-" + version + ".jar");
		move("ui/target/daccosim-editor-jar-with-dependencies.jar", "ui/target/daccosim-editor-" + year + "-" + version + ".jar");
		move("ui/target/daccosim-editor-32bits.exe", "ui/target/daccosim-editor-" + year + "-" + version + "-32bits.exe");
		move("ui/target/daccosim-editor-64bits.exe", "ui/target/daccosim-editor-" + year + "-" + version + "-64bits.exe");
		System.out.println("Files renamed for distribute");
	}

	private static void move(String path, String newPath) {
		try {
			Files.move(new File(wd + path).toPath(), new File(wd + newPath).toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void deployToBitBucket() {
		try {
			System.out.println("Write <user>:<password> for bitbucket");
			String userPass = new Scanner(System.in).nextLine();
			if (userPass.isEmpty()){
				System.out.println("Cancelled upload to BitBucket");
				return;
			}
			uploadFile(userPass, wd + "shell/target/daccosim-shell-" + year + "-" + version + ".jar");
			uploadFile(userPass, wd + "ui/target/daccosim-editor-" + year + "-" + version + ".jar");
			uploadFile(userPass, wd + "ui/target/daccosim-editor-" + year + "-" + version + "-32bits.exe");
			uploadFile(userPass, wd + "ui/target/daccosim-editor-" + year + "-" + version + "-64bits.exe");
			System.out.println("Uploaded all files to bitbucket");
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	}

	private static void uploadFile(String userPass, String file) throws InterruptedException, IOException {
		execProcess("curl -u " + userPass + " -X POST https://api.bitbucket.org/2.0/repositories/simulage/daccosim/downloads -F files=@" + file);
		System.out.println("File uploaded: " + file);
	}

	static class StreamGobbler extends Thread {

		InputStream is;

		StreamGobbler(InputStream is) {
			this.is = is;
		}

		public void run() {
			try {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line;
				while ((line = br.readLine()) != null)
					System.out.println(line);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}
}