/*
 *  Copyright 2013-2018 - Monentia
 *
 *   Daccosim is a collaborative development effort between EDF (France),
 *   CentraleSupélec (France), EIFER Institute (Germany), SIANI institute (Spain)
 *   and Monentia S.L. (Spain)
 *
 *  This File is part of Daccosim Project
 *
 * Daccosim Project is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License.
 *
 *  Daccosim Project is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Daccosim. If not, see <http://www.gnu.org/licenses/>.
 */

package eu.simulage.shell;

import eu.simulage.daccosim.DaccosimException;
import eu.simulage.daccosim.GraphExecutor;
import eu.simulage.daccosim.view.FMU;
import eu.simulage.daccosim.view.Graph;
import org.javafmi.kernel.Unzipper;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static eu.simulage.daccosim.DngFile.importDng;
import static eu.simulage.daccosim.GraphExecutor.logger;
import static eu.simulage.daccosim.view.Graph.from;
import static java.util.Arrays.stream;
import static org.javafmi.kernel.Files.deleteAll;

public class Main {

	private static File file;
	private static File workingDirectory;

	@SuppressWarnings("ConstantConditions")
	public static void main(String[] args) throws IOException {
		if (args.length != 1) {
			System.out.println("Path to .dng .dsg .simx or .zip file must be provided");
			return;
		}
		file = new File(args[0]);
		workingDirectory = new File(args[0]).getParentFile();
		String filename = file.getName().toLowerCase();
		Graph graph = filename.endsWith("dng") ? importDng(file) :
				filename.endsWith("simx") ? importSimx(file) :
						filename.endsWith("zip") ? importZip(file) :
								from(file);
		checkPaths(file, graph);
		if (graph.export() != null && graph.export().prefix().isEmpty())
			graph.export().prefix(file.getName().substring(0, file.getName().lastIndexOf(".")));
		new GraphExecutor(graph.commit()).execute();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			if (args[0].endsWith("simx")) deleteAll(file.getParentFile());
		}));
	}

	private static Graph importSimx(File file) {
		try {
			logger().info("Loading simx");
			Path directory = Files.createTempDirectory("simx");
			new Unzipper(file).unzipAll(directory.toFile());
			File dsgFile = stream(directory.toFile().listFiles()).filter(f -> f.getName().endsWith(".dsg")).findFirst().orElseThrow(() -> new DaccosimException("Simx file format is invalidad. Simx file must contain a .dsg file"));
			Main.file = dsgFile;
			logger().info("simx loaded");
			return from(dsgFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static Graph importZip(File file) {
		try {
			logger().info("Unzipping ZIP");
			Path directory = Files.createTempDirectory("zip");
			new Unzipper(file).unzipAll(directory.toFile());
			File dngFile = stream(directory.toFile().listFiles()).filter(f -> f.getName().endsWith(".dng")).findFirst().get();
			Main.file = dngFile;
			logger().info("ZIP unzipped");
			return importDng(dngFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void checkPaths(File file, Graph graph) {
		if (filesAreAbsolute(graph)) return;
		graph.nodes().stream().filter(n -> n instanceof FMU).map(n -> (FMU) n)
				.forEach(f -> f.path(new File(file.getParentFile(), f.path()).getAbsolutePath()));
		if (graph.export() != null) graph.export().folder(workingDirectory.getAbsolutePath());
	}

	private static boolean filesAreAbsolute(Graph graph) {
		return graph.nodes().stream().filter(n -> n instanceof FMU)
				.map(n -> (FMU) n)
				.anyMatch(f -> new File(f.path()).isAbsolute());
	}

}
